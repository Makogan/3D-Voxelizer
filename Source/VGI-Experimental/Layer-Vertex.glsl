#version 430

const vec2 vertices[4] = {vec2(-1.0,-1.0),vec2(1.0,-1.0),vec2(-1.0,1.0),vec2(1.0,1.0)};

out vec2 f_coord;

void main()
{
    gl_Position = vec4(vertices[gl_VertexID], 0.0, 1.0);
    f_coord = gl_Position.xy*0.5+vec2(0.5);
}