#version 430

in vec2 f_coord;

out vec4 color;

uniform layout(binding=2) sampler2D layer;

void main()
{
    color = texture(layer, f_coord);
}