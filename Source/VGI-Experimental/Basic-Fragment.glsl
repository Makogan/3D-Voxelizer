//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief A simple fragment shader to render 3D objects
 * 
 * @file Basic-Fragment.glsl
 * @author Camilo Talero
 * @date 2018-04-24
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#version 430

in vec3 f_pos;
in vec3 f_norm;
in vec3 f_uv;

out vec4 fragment_color;

vec3 light = vec3(20,20,20);

uniform vec3 camera_position;
uniform layout(binding=0) sampler2DArray text;

layout(std430, binding = 2) buffer texture_meta_data
{
	vec2 stretches[];
};

vec4 blinn_phong()
{
    vec3 pos = f_pos;

	vec4 color = vec4(0);
	vec3 l = vec3(light-f_pos);
	if(length(l)>0)
		l = normalize(l);
	int image=int(round(f_uv.z));
	float x_stretch = stretches[image].x, y_stretch = stretches[image].y;
	float u = mod(f_uv.x*x_stretch, x_stretch);
	float v = mod(f_uv.y*y_stretch, y_stretch);
    vec3 c = vec3(texture(text, vec3(u,v,image)));

	vec3 n = normalize(f_norm);
	vec3 e = camera_position-f_pos;
	e = normalize(e);
	vec3 h = normalize(e+l);

	color = vec4(c*(vec3(0.5)+0.5*max(0,dot(n,l))) +
		vec3(0.1)*max(0,pow(dot(h,n), 100)), 1);

    return color;

}

void main()
{
    fragment_color = blinn_phong();
}