#version 430

//In variables
layout (triangles) in;
layout (triangle_strip, max_vertices=136) out;

in vec3 v_pos[]; //3D position
in vec3 v_norm[];//normal to the vertex
in vec3 v_uv[]; //Texture coordinates

//out variables
out vec3 f_pos; //Final position of the fragment in 3D
out vec3 f_norm;//Normal of the fragment
out vec3 f_uv;  //Texture coordinates

uniform vec3 camera_position;
uniform vec3 camera_direction;

uniform mat4 view_m = mat4(1);  // view matrix
uniform mat4 proj_m = mat4(1);  // perspective projection matrix

void main()
{
    bool visible = false;
    vec3 center = vec3(0);
    for(uint i=0; i<3; i++)
        center+=v_pos[i]/3;

    for(uint i=0; i<3; i++)
    {
        vec3 d1 = normalize(v_pos[i]-camera_position);
        //d1.z=0;
        //d1=normalize(d1);
        vec3 d2 = normalize(camera_direction);
        //d2.z=0;
        //d2=normalize(d2);
        visible = visible || (acos(dot(d1, d2)) <= radians(45)*1.1);

        d1 = normalize(center-camera_position);
        visible = visible || (acos(dot(d1, d2)) <= radians(45)*1.1);
    }

    //if(visible)
    {
        for(uint i=0; i<3; i++)
        {
            f_pos = v_pos[i];
            f_norm = v_norm[i];
            f_uv = v_uv[i];

            gl_Position = gl_in[i].gl_Position;

            EmitVertex();
        }
        EndPrimitive();
    }
}