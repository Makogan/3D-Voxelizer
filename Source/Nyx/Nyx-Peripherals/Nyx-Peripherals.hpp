//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Header declaration of classes to wrap GLFW input calls
 *
 * @file Nyx-Peripherals.hpp
 * @author Camilo Talero
 * @date 2018-05-02
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================
#pragma once

#include "Nyx-Window.hpp"
namespace Nyx{
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                    Keyboard Class                                    *
 *                                                                                      */
//========================================================================================
/**
 * @brief Wrapper class to manage keyboard input
 *
*/
class Nyx_Keyboard
{
//──── Private Members ───────────────────────────────────────────────────────────────────

    private:
        GLFWwindow* window; //!< Pointer to a GLFW window this keyboard is attached to
        /**
         * @name Key functions
         *
         * @brief Each of the following functions will be called when the associated key
         * is pressed
         *
        */
        ///@{
        void(*func_0)();
        void(*func_1)();
        void(*func_2)();
        void(*func_3)();
        void(*func_4)();
        void(*func_5)();
        void(*func_6)();
        void(*func_7)();
        void(*func_8)();
        void(*func_9)();

        void(*a_func)();
        void(*d_func)();
        void(*s_func)();
        void(*w_func)();
        void(*shift_func)();
        void(*space_func)();
        ///@}

//──── Public Members ────────────────────────────────────────────────────────────────────

    public:
//──── Constructors & Destructors ────────────────────────────────────────────────────────

        /**
         * @brief Construct a new Nyx_Keyboard object
         *
         * @param window pointer to the window this keyboard will be attached to
        */
        Nyx_Keyboard(Nyx_Window *window);

//──── Other Functions ───────────────────────────────────────────────────────────────────

        /**
         * @name Key setters
         *
         * @brief Each of the following functions will associate a function with a key
         *
        */
        ///@{
        void set_0_func(void(*f)()){func_0 = f;}
        void set_1_func(void(*f)()){func_1 = f;}
        void set_2_func(void(*f)()){func_2 = f;}
        void set_3_func(void(*f)()){func_3 = f;}
        void set_4_func(void(*f)()){func_4 = f;}
        void set_5_func(void(*f)()){func_5 = f;}
        void set_6_func(void(*f)()){func_6 = f;}
        void set_7_func(void(*f)()){func_7 = f;}
        void set_8_func(void(*f)()){func_8 = f;}
        void set_9_func(void(*f)()){func_9 = f;}

        void set_a_func(void(*f)()){a_func = f;}
        void set_d_func(void(*f)()){d_func = f;}
        void set_s_func(void(*f)()){s_func = f;}
        void set_w_func(void(*f)()){w_func = f;}

        void set_shift_func(void(*f)()){shift_func = f;}
        void set_space_func(void(*f)()){space_func = f;}
        ///@}
        /**
         * @brief Call the functions for each pressed key
         *
        */
        void updateAllKeys();

};
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                      Mouse Class                                     *
 *                                                                                      */
//========================================================================================

/**
 * @brief Class to manage mouse input
 * 
*/
class Nyx_Mouse
{
//──── Private Members ───────────────────────────────────────────────────────────────────
    private:

        GLFWwindow* window; //<! Pointer to the current window

//──── Public Members ────────────────────────────────────────────────────────────────────

    public:

//──── Constructors & Destructors ────────────────────────────────────────────────────────

        /**
         * @brief Construct a new Nyx_Mouse object
         *
         * @param w Pointer to the window this object is attached to
        */
        Nyx_Mouse(Nyx_Window *w)
        {
            window = w->getWindowPtr();
            glfwMakeContextCurrent(window);
        };

//──── Other Functions ───────────────────────────────────────────────────────────────────

        /**
         * @brief Set the cursor position callback function
         *
         * @param f The function that will be called on mouse position events
        */
        void inline set_pos_func(void(*f)(GLFWwindow*, double, double))
        {
            glfwSetCursorPosCallback(window, f);
        }
        /**
         * @brief Disable cursors in the window
         *
        */
        void inline disable_cursor()
        {glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);}
};

//########################################################################################
}//Close Nyx namespace

