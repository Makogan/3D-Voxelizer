//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Implementation of the Peripheral wrapping classes
 * 
 * @file Nyx-Peripherals.cpp
 * @author Camilo Talero
 * @date 2018-05-02
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================

#include "Nyx-Peripherals.hpp"
namespace Nyx{
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                  Nyx Keyboard Class                                  *
 *                                                                                      */
//========================================================================================

Nyx_Keyboard::Nyx_Keyboard(Nyx_Window *w)
{
    window = w->getWindowPtr();

    glfwMakeContextCurrent(window);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);
}

void Nyx_Keyboard::updateAllKeys()
{
    if(glfwGetKey(window, GLFW_KEY_0)==GLFW_PRESS)
        func_0();

    if(glfwGetKey(window, GLFW_KEY_1)==GLFW_PRESS)
        func_1();

    if(glfwGetKey(window, GLFW_KEY_2)==GLFW_PRESS)
        func_2();

    if(glfwGetKey(window, GLFW_KEY_3)==GLFW_PRESS)
        func_3();

    if(glfwGetKey(window, GLFW_KEY_4)==GLFW_PRESS)
        func_4();

    if(glfwGetKey(window, GLFW_KEY_5)==GLFW_PRESS)
        func_5();

    if(glfwGetKey(window, GLFW_KEY_6)==GLFW_PRESS)
        func_6();

    if(glfwGetKey(window, GLFW_KEY_7)==GLFW_PRESS)
        func_7();

    if(glfwGetKey(window, GLFW_KEY_8)==GLFW_PRESS)
        func_8();

    if(glfwGetKey(window, GLFW_KEY_9)==GLFW_PRESS)
        func_9();

    if(glfwGetKey(window, GLFW_KEY_S)==GLFW_PRESS)
        s_func();

    if(glfwGetKey(window, GLFW_KEY_A)==GLFW_PRESS)
        a_func();

    if(glfwGetKey(window, GLFW_KEY_D)==GLFW_PRESS)
        d_func();

    if(glfwGetKey(window, GLFW_KEY_W)==GLFW_PRESS)
        w_func();

    if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)==GLFW_PRESS)
        shift_func();

    if(glfwGetKey(window, GLFW_KEY_SPACE)==GLFW_PRESS)
        space_func();
}
}//CLose Nyx namespace
//########################################################################################