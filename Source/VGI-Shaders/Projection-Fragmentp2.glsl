//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief A simple fragment shader to render 3D objects
 * 
 * @file Basic-Fragment.glsl
 * @author Camilo Talero
 * @date 2018-04-24
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#version 430

#define PI 3.141592653589793

in vec3 f_pos;
in vec3 f_norm;
in vec3 f_uv;

out vec4 fragment_color;

vec3 light = vec3(20,20,20);

uniform vec3 camera_position;

uniform float cube_dim;
uniform int voxel_resolution;

uniform layout(binding=0) sampler3D diffuse_map;
uniform layout(binding=1) sampler3D normal_map;

uniform layout(binding=2) sampler2DArray text;
layout(std430, binding = 2) buffer texture_meta_data
{
	vec2 stretches[];
};

// Information of a voxel (a tree leaf)
struct Voxel
{
    vec4 normal;
    vec4 color;
};
// A non-leaf nod in the tree
struct Node
{
    int children[8];
};
//Buffer for the leafs of the tree
layout(std430, binding = 0) buffer voxel_buffer
{
    uint index;
    Voxel voxels[];
};
//Buffer for the rest of the tree
layout(std430, binding = 1) buffer tree_buffer
{
    uint t_index;
    Node tree[];
};

#define EPSILON 0.01
// Check whether the position is inside of the specified box
bool inBoxBounds(vec3 corner, float size, vec3 position)
{
    bool inside = true;
    //Put the position in the coordinate frame of the box
    position-=corner;
    //The point is inside only if all of it's components are inside
    for(int i=0; i<3; i++)
    {
        inside = inside && (position[i] > -EPSILON);
        inside = inside && (position[i] < size+EPSILON);
    }

    return inside;
}
//Calculate the distance to the intersection to a box, or inifnity if the bos cannot be hit
float boxIntersection(vec3 origin, vec3 dir, vec3 corner0, float size)
{
    //calculate opposite corner
    vec3 corner1 = corner0 + vec3(size,size,size);

    //Set the ray plane intersections
    float coeffs[6];
    coeffs[0] = (corner0.x - origin.x)/(dir.x);
    coeffs[1] = (corner0.y - origin.y)/(dir.y);
    coeffs[2] = (corner0.z - origin.z)/(dir.z);
    coeffs[3] = (corner1.x - origin.x)/(dir.x);
    coeffs[4] = (corner1.y - origin.y)/(dir.y);
    coeffs[5] = (corner1.z - origin.z)/(dir.z);

    float t = 1.f/0.f;
    //Check for the smallest valid intersection distance
    //We allow negative values up to -size to create correct sorting if the origin is
    //inside the box
    for(uint i=0; i<6; i++)
        t = coeffs[i]>=-size && inBoxBounds(corner0,size,origin+dir*coeffs[i])?
            min(coeffs[i],t) : t;

    return t;
}
//Compute the relative corner coordinate based on the child value (0-7)
vec3 compute_corner(int val, float size)
{
    vec3 corner;

    corner.x = float((val & 4)>>2)*size;
    corner.y = float((val & 2)>>1)*size;
    corner.z = float((val & 1))*size;

    return corner;
}
#define MAX_TREE_HEIGHT 20
//Set the stack
int nodes[MAX_TREE_HEIGHT];
int levels[MAX_TREE_HEIGHT];
vec3 positions[MAX_TREE_HEIGHT];

int sample_oct_tree(vec3 origin, vec3 dir)
{
    int sp=1;
    //Count for the number of iterations
    int count = 0;

     //Initialize the variables for the algorithm
    int max_level = int(log2(voxel_resolution));
    nodes[0]=0;
    levels[0]=0;
    positions[0]=vec3(-cube_dim);
    int level=0;
    int node=0;
    float stop = length(origin-light);
    float travelled = 0;

    do
    {
        vec3 corner;
        //pop the stack for the current values
        sp--;
        node = nodes[sp];
        level = levels[sp];
        corner = positions[sp];
        //Cacluate the size fo the children
        float size = cube_dim / (1 << level);
        //Calculate intersections with the 8 children, a coefficient may be engative up to
        //-size to handle sorting of the coefficients if the starting positin is inside a box
        float coeffs[8];
        for(int child=0; child<8; child++)
        {
            //Only non-empty children can be collided with
            coeffs[child] = tree[node].children[child]>0?
                boxIntersection(origin, dir, corner + compute_corner(child, size), size) : 1.f/0.f;
        }
        //Bubble sort the distances from closest to farthest
        int indices[8] = {0,1,2,3,4,5,6,7};
        for(uint i=0; i<8; i++)
        {
            for(uint j=i; j<8; j++)
            {
                if((coeffs[j] <= coeffs[i]))
                {
                    float swap = coeffs[i];
                    coeffs[i] = coeffs[j];
                    coeffs[j] = swap;

                    int iSwap = indices[i];
                    indices[i] = indices[j];
                    indices[j] = iSwap;
                }
            }
        }
        travelled = coeffs[0];
        //Push to the stack in reverse order (push farthest first, closest last)
        for(int i=7; i>=0; i--)
        {
            if(!isinf(coeffs[i]))
            {
                nodes[sp] = tree[node].children[indices[i]];
                levels[sp] = level+1;
                positions[sp] = corner + compute_corner(indices[i], size);
                sp++;
            }
        }
        count++;
        //Stop the algorithm if we have exceeded the limit number of iterations
        if(count > 100)
        {
            return -1;
        }
        if(travelled >= stop && !isinf(travelled))
        {
            //fragment_color = vec4(0,0,1,0);
            return -1;
        }
    //Terminate the loop if we hit the last level, the stack is empty or we have no more
    //space in the stack
    }while(level < (max_level-1) && (sp>0) && (sp<=MAX_TREE_HEIGHT));

    //fragment_color = vec4(travelled)/10000;

    //Set the color of the fragment
    if(level==max_level-1)
    {
        //fragment_color = vec4(1,0,0,0);
        return node;
    }
    return -1;

}

vec4 blinn_phong()
{
    vec3 pos = f_pos;

	vec4 color = vec4(0);
	vec3 l = vec3(light-f_pos);
	if(length(l)>0)
		l = normalize(l);
	int image=int(round(f_uv.z));
	float x_stretch = stretches[image].x, y_stretch = stretches[image].y;
	float u = mod(f_uv.x*x_stretch, x_stretch);
	float v = mod(f_uv.y*y_stretch, y_stretch);
    vec3 c = vec3(texture(text, vec3(u,v,image)));

	vec3 n = normalize(f_norm);
	vec3 e = camera_position-f_pos;
	e = normalize(e);
	vec3 h = normalize(e+l);

	color = vec4(c*(vec3(0.5)+0.5*max(0,dot(n,l))) +
		vec3(0.1)*max(0,pow(dot(h,n), 100)), 1);

    return color;

}

void main()
{
    fragment_color = blinn_phong();

    float v_size = cube_dim/voxel_resolution;
	vec3 r = f_pos + f_norm*v_size*10;
	vec3 dir = normalize(light-r);
    vec3 start = r;

    int node = sample_oct_tree(r, dir);
    if(node != -1)
        fragment_color*=0.2;
}