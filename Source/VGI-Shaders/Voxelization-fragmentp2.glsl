#version 430

//Input variables
in vec3 f_pos;
in vec3 f_norm;
in vec3 f_uv;

// Information of a voxel (a tree leaf)
struct Voxel
{
    vec4 normal;
    vec4 color;
};
// A non-leaf nod in the tree
struct Node
{
    int children[8];
};
//Buffer for the leafs of the tree
layout(std430, binding = 0) buffer voxel_buffer
{
    uint index;
    Voxel voxels[];
};
//Buffer for the rest of the tree
layout(std430, binding = 1) buffer tree_buffer
{
    uint t_index;
    Node tree[];
};

uniform layout(binding=2) sampler2DArray text;
layout(std430, binding = 2) buffer texture_meta_data
{
	vec2 stretches[];
};

uniform int voxel_resolution;
uniform float cube_dim;

//Associate a number from 0 to 7 to a position
//The position must be inside a box and expressed in the coordinate system fo the box
int getVIndex(vec3 position, int level)
{
    float size = cube_dim / pow(2,level);

    int bit2 = int(position.x > size);
    int bit1 = int(position.y > size);
    int bit0 = int(position.z > size);

    return 4*bit2 + 2*bit1 + bit0;
}
//Compute the relative corner coordinate based on the child value (0-7)
vec3 compute_corner(int val, float size)
{
    vec3 corner;

    corner.x = float((val & 4)>>2)*size;
    corner.y = float((val & 2)>>1)*size;
    corner.z = float((val & 1))*size;

    return corner;
}

void main()
{
    //Set the index for the current voxel and allocate its memory
    uint m_index = atomicAdd(index, 1);
    //Set teh voxel vlaues
    vec3 position = f_pos;
    voxels[m_index].normal = vec4(f_norm,1);

    int layer=int(round(f_uv.z));
	float x_stretch = stretches[layer].x, y_stretch = stretches[layer].y;
	float u = mod(f_uv.x*x_stretch, x_stretch);
	float v = mod(f_uv.y*y_stretch, y_stretch);
    voxels[m_index].color = texture(text, vec3(u,v,layer));
    //Calculate the height of the tree
    int max_level = int(log2(voxel_resolution));
    //Initialize the algorithm variables
    int node = 0;
    vec3 corner = vec3(-cube_dim);
    int child;
    int pnode;
    int pchild;
    //Travel down the tree from theroot to the leaves
    for(int level=0; level<max_level-1; level++)
    {
        //Calcualte the size of a child box at the current level
        float size = cube_dim / float(int(1<<level));
        //Get the child index of the current position
        child = getVIndex(position-corner, level);

        int mrun = 100; //Limti the number of iterations waiting for the node to be released
        //Lock too coordinate competing threads
        while ((tree[node].children[child] <= 0) && (mrun > 0)){
            mrun--;
            //If the node wasn't allocated, allocate its memory (which also releases the lock)
            if( (atomicCompSwap( tree[node].children[child] , 0 , -1) == 0 ))
            {
                tree[node].children[child] = int(atomicAdd(t_index, 1));
            }
        }
        //Discard fragments that ended due to limting conditions
        if(mrun < 1)
            discard;
        //Store the information fo the parent
        pnode = node;
        pchild = child;
        //Update the iteration variables
        node = tree[node].children[child];
        corner += compute_corner(child, size);
    }
    //Store the information of the last child into the tree
    tree[pnode].children[pchild] = int(m_index);
}