#version 430

in vec2 f_coord;

out vec4 fragment_color;

// Information of a voxel (a tree leaf)
struct Voxel
{
    vec4 normal;
    vec4 color;
};
// A non-leaf nod in the tree
struct Node
{
    int children[8];
};
//Buffer for the leafs of the tree
layout(std430, binding = 0) buffer voxel_buffer
{
    uint index;
    Voxel voxels[];
};
//Buffer for the rest of the tree
layout(std430, binding = 1) buffer tree_buffer
{
    uint t_index;
    Node tree[];
};

uniform vec3 camera_pos;        //Position of the camera in 3D
uniform float aspect_ratio;     //Aspect ratio of the current window
uniform float cube_dim;         //Dimension of the cube (side length is 2*cube_dim)
uniform int voxel_resolution;   //The relaive size of a voxel in the cube
uniform mat4 view_m = mat4(1);  // view matrix

#define EPSILON 0.01
// Check whether the position is inside of the specified box
bool inBoxBounds(vec3 corner, float size, vec3 position)
{
    bool inside = true;
    //Put the position in the coordinate frame of the box
    position-=corner;
    //The point is inside only if all of it's components are inside
    for(int i=0; i<3; i++)
    {
        inside = inside && (position[i] > -EPSILON);
        inside = inside && (position[i] < size+EPSILON);
    }

    return inside;
}
//Calculate the distance to the intersection to a box, or inifnity if the bos cannot be hit
float boxIntersection(vec3 origin, vec3 dir, vec3 corner0, float size)
{
    //calculate opposite corner
    vec3 corner1 = corner0 + vec3(size,size,size);

    //Set the ray plane intersections
    float coeffs[6];
    coeffs[0] = (corner0.x - origin.x)/(dir.x);
    coeffs[1] = (corner0.y - origin.y)/(dir.y);
    coeffs[2] = (corner0.z - origin.z)/(dir.z);
    coeffs[3] = (corner1.x - origin.x)/(dir.x);
    coeffs[4] = (corner1.y - origin.y)/(dir.y);
    coeffs[5] = (corner1.z - origin.z)/(dir.z);

    float t = 1.f/0.f;
    //Check for the smallest valid intersection distance
    //We allow negative values up to -size to create correct sorting if the origin is
    //inside the box
    for(uint i=0; i<6; i++)
        t = coeffs[i]>=-size && inBoxBounds(corner0,size,origin+dir*coeffs[i])?
            min(coeffs[i],t) : t;

    return t;
}
//Compute the relative corner coordinate based on the child value (0-7)
vec3 compute_corner(int val, float size)
{
    vec3 corner;

    corner.x = float((val & 4)>>2)*size;
    corner.y = float((val & 2)>>1)*size;
    corner.z = float((val & 1))*size;

    return corner;
}
#define MAX_TREE_HEIGHT 12
//Set the stack
int nodes[MAX_TREE_HEIGHT];
int levels[MAX_TREE_HEIGHT];
vec3 positions[MAX_TREE_HEIGHT];

int sample_oct_tree(vec3 origin, vec3 dir)
{
    int sp=1;
    //Count for the number of iterations
    int count = 0;

     //Initialize the variables for the algorithm
    int max_level = int(log2(voxel_resolution));
    nodes[0]=0;
    levels[0]=0;
    positions[0]=vec3(-cube_dim);
    int level=0;
    int node=0;

    do
    {
        vec3 corner;
        //pop the stack for the current values
        sp--;
        node = nodes[sp];
        level = levels[sp];
        corner = positions[sp];
        //Cacluate the size fo the children
        float size = cube_dim / (1 << level);
        //Calculate intersections with the 8 children, a coefficient may be engative up to
        //-size to handle sorting of the coefficients if the starting positin is inside a box
        float coeffs[8];
        for(int child=0; child<8; child++)
        {
            //Only non-empty children can be collided with
            coeffs[child] = tree[node].children[child]>0?
                boxIntersection(origin, dir, corner + compute_corner(child, size), size) : 1.f/0.f;
        }
        //Bubble sort the distances from closest to farthest
        int indices[8] = {0,1,2,3,4,5,6,7};
        for(uint i=0; i<8; i++)
        {
            for(uint j=i; j<8; j++)
            {
                if((coeffs[j] <= coeffs[i]))
                {
                    float swap = coeffs[i];
                    coeffs[i] = coeffs[j];
                    coeffs[j] = swap;

                    int iSwap = indices[i];
                    indices[i] = indices[j];
                    indices[j] = iSwap;
                }
            }
        }
        //Push to the stack in reverse order (push farthest first, closest last)
        for(int i=7; i>=0; i--)
        {
            if(!isinf(coeffs[i]))
            {
                nodes[sp] = tree[node].children[indices[i]];
                levels[sp] = level+1;
                positions[sp] = corner + compute_corner(indices[i], size);
                sp++;
            }
        }
        count++;
        //Stop the algorithm if we have exceeded the limit number of iterations
        if(count > 100)
            break;
    //Terminate the loop if we hit the last level, the stack is empty or we have no more
    //space in the stack
    }while(level < (max_level-1) && (sp>0) && (sp<=MAX_TREE_HEIGHT));

    //fragment_color = vec4(count)/100;

    //Set the color of the fragment
    if(level==max_level-1)
        return node;
    return -1;

}

void main()
{
    //Initialize the ray
    vec3 r = vec3(f_coord.x, f_coord.y, 1.f/tan(radians(40)));
    r.x *=-1;
    r.y/=aspect_ratio;
    vec3 dir = normalize(vec3(view_m*vec4(r,1)));
    r += vec3(0,0,-1.f/tan(radians(40))) + camera_pos;

    fragment_color = vec4(0);

    int node = sample_oct_tree(r, dir);
    if(node != -1)
        fragment_color = (voxels[node].color);
}