#version 430

in vec3 f_pos;
in vec3 f_norm;
in vec3 f_uv;

uniform layout(binding=2) sampler2DArray text;
uniform layout(binding=0, rgba8) image3D diffuse_map;
uniform layout(binding=1, rgba8) image3D normal_map;
layout(std430, binding = 2) buffer texture_meta_data
{
	vec2 stretches[];
};

out vec4 fragment_color;

uniform int voxel_resolution;
uniform float cube_dim;

void main()
{
    vec3 pos = f_pos/cube_dim;
    int layer=int(round(f_uv.z));
	float x_stretch = stretches[layer].x, y_stretch = stretches[layer].y;
	float u = mod(f_uv.x*x_stretch, x_stretch);
	float v = mod(f_uv.y*y_stretch, y_stretch);
    vec4 t_color = texture(text, vec3(u,v,layer));

    imageStore(diffuse_map, ivec3(((pos)*0.5+vec3(0.5))*(voxel_resolution)), t_color);
    imageStore(normal_map, ivec3(((pos)*0.5+vec3(0.5))*(voxel_resolution)), abs(vec4(f_norm,1)));
}
