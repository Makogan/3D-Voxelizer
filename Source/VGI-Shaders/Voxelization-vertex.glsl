#version 430

layout(location = 0) in vec3 position;  // (x,y,z) coordinates of a vertex
layout(location = 1) in vec3 normal;    // normal to the vertex
layout(location = 2) in vec3 uv;        // texture coordinates

out vec3 v_pos;
out vec3 v_norm;
out vec3 v_uv;

uniform mat4 model_m = mat4(1); // model matrix
uniform mat4 view_m = mat4(1);  // view matrix

void main()
{
    //modify the information accordingly
    v_pos = vec3(model_m*vec4(position,1));
    v_norm = vec3(model_m*vec4(normal,1.0));
    v_uv = uv;
}