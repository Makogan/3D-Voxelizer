#version 430

#define PI 3.141592653589793

in vec2 f_coord;

out vec4 fragment_color;

uniform layout(binding=0) sampler3D diffuse_map;
uniform layout(binding=1) sampler3D normal_map;

uniform vec3 camera_pos;
uniform float aspect_ratio;
uniform float cube_dim;
uniform int voxel_resolution;

vec3 light = vec3(0, 600, 0);

#define EPSILON 0.01
// Check whether the position is inside of the specified box
bool inBoxBounds(vec3 corner, float size, vec3 position)
{
    bool inside = true;
    //Put the position in the coordinate frame of the box
    position-=corner;
    //The point is inside only if all of it's components are inside
    for(int i=0; i<3; i++)
    {
        inside = inside && (position[i] > -EPSILON);
        inside = inside && (position[i] < size+EPSILON);
    }

    return inside;
}
//Calculate the distance to the intersection to a box, or inifnity if the bos cannot be hit
float boxIntersection(vec3 origin, vec3 dir, vec3 corner0, float size)
{
    //calculate opposite corner
    vec3 corner1 = corner0 + vec3(size,size,size);

    //Set the ray plane intersections
    float coeffs[6];
    coeffs[0] = (corner0.x - origin.x)/(dir.x);
    coeffs[1] = (corner0.y - origin.y)/(dir.y);
    coeffs[2] = (corner0.z - origin.z)/(dir.z);
    coeffs[3] = (corner1.x - origin.x)/(dir.x);
    coeffs[4] = (corner1.y - origin.y)/(dir.y);
    coeffs[5] = (corner1.z - origin.z)/(dir.z);

    float t = 1.f/0.f;
    //Check for the smallest valid intersection distance
    //We allow negative values up to -size to create correct sorting if the origin is
    //inside the box
    for(uint i=0; i<6; i++)
        t = (coeffs[i]>=0) && inBoxBounds(corner0,size,origin+dir*coeffs[i])?
            min(coeffs[i],t) : t;

    return t;
}

bool incube(vec3 pos, float size)
{
    return
        (pos.x >= -size - EPSILON) && (pos.x <= size + EPSILON) &&
        (pos.y >= -size - EPSILON) && (pos.y <= size + EPSILON) &&
        (pos.z >= -size - EPSILON) && (pos.z <= size + EPSILON);
}

uniform mat4 view_m = mat4(1);  // view matrix

vec4 blinn_phong(vec3 pos, vec3 norm, vec4 color)
{
	vec3 l = vec3(light-pos);
	if(length(l)>0)
		l = normalize(l);

	vec3 n = normalize(norm);
	vec3 e = camera_pos-pos;
	e = normalize(e);
	vec3 h = normalize(e+l);
    vec3 c = vec3(color);
	color = vec4(c*(vec3(0.5)+0.5*max(0,dot(n,l))) +
		vec3(0.1)*max(0,pow(dot(h,n), 100)), 1);

    return color;

}

void main()
{
    float v_size = cube_dim/voxel_resolution;
    vec3 r = (vec3(f_coord.xy,1.f/tan(radians(45))));
    r.x*=-1;
    r.y /= aspect_ratio;
    vec3 dir = vec3(view_m*vec4(normalize(r),0));
    dir = normalize(dir);
    r = camera_pos;

    float t = boxIntersection(r, dir, -vec3(cube_dim), cube_dim*2);

    if(isinf(t))
        discard;

    if(!isinf(boxIntersection(r,dir, vec3(light)-vec3(5.f), 10.f)))
    {
        fragment_color = vec4(1,0,1,1);
        return;
    }

    if(!incube(r, cube_dim))
        r += dir*t;

    vec4 color = vec4(0);
    while(incube(r, cube_dim))
    {
        r += dir*v_size*0.5;
        vec4 val = texelFetch(normal_map, ivec3(((r)*0.5/cube_dim+vec3(0.5))*(voxel_resolution-1)),0);
        if(val.w > 0)
        {
            break;
        }
    }
    vec4 normal = texelFetch(normal_map, ivec3(((r)*0.5/cube_dim+vec3(0.5))*(voxel_resolution-1)),0);
    color = texelFetch(diffuse_map, ivec3(((r)*0.5/cube_dim+vec3(0.5))*(voxel_resolution-1)),0);

    fragment_color = color;//blinn_phong(r, vec3(normal), color)*(10.f/log(length(r-light)));

    /*dir = normalize(light-r);
    vec3 start = r;
    r += dir;
    float hit = 1.f;
    while(incube(r, cube_dim) && length(start-r)<=length(start-light))
    {
        r += dir*v_size*0.5;
        vec4 val = texelFetch(normal_map, ivec3(((r)*0.5/cube_dim+vec3(0.5))*(voxel_resolution-1)));
        if(val.w > 0)
        {
            hit=atan(length(start-r)/200.f)/(PI/2.f)*0.9 + 0.1;
            break;
        }
    }

    fragment_color *=hit; //vec4(length(r-start)/1000.f,0,0,1);*/
}
