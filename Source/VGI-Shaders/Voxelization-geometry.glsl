#version 430

//In variables
layout (triangles) in;
layout (triangle_strip, max_vertices=136) out;

in vec3 v_pos[]; //3D position
in vec3 v_norm[];//normal to the vertex
in vec3 v_uv[]; //Texture coordinates

//out variables
out vec3 f_pos; //Final position of the fragment in 3D
out vec3 f_norm;//Normal of the fragment
out vec3 f_uv;  //Texture coordinates

//Dimension of the cube in 3D, (side lenght of the cube will be cube_dim*2)
uniform float cube_dim=1;

//Create a 3D arbitrary rotation matrix
mat3 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c         );
}

void main()
{
    //Calculate 2 sides of the triangle
    vec3 side1 = v_pos[0] - v_pos[1];
    vec3 side2 = v_pos[1] - v_pos[2];
    //Determine the normal to the triangle
    vec3 tNorm = normalize(cross(side1,side2));
    //Calculate the center of mass of the triangle
    vec3 center = vec3(0);
    for(uint i=0; i<3; i++)
        center+=v_pos[i]/3;
    //For every vertex
    tNorm = abs(tNorm);
    for(uint i=0; i<3; i++)
    {
        //Set the fragment out variables
        f_pos = v_pos[i];
        f_norm = v_norm[i];
        f_uv = v_uv[i];

        vec3 pos = f_pos/cube_dim;

       /* //Determine the angle of rotation to orient the triangle parallel to the xy plane
        float angle = acos(dot(vec3(0,0,1), tNorm));
        //Determine the axis of rotation
        vec3 axis = cross(tNorm, vec3(0,0,1));
        //Calculate the rotation matrix
        mat3 rot = mat3(1);
        if(angle>0.001)
            rot = rotationMatrix(axis, -angle);

        //Move the center of mass of the triangle to the origin, rotate it, scale it
        //then offset it to its scaled position in the xy plane
        vec3 flatPos = rot * (v_pos[i] - center)/cube_dim + center/cube_dim;
        gl_Position = vec4(flatPos.x, flatPos.y, 0, 1);*/

        if(tNorm.x>tNorm.y && tNorm.x>tNorm.z)
        {
            gl_Position = vec4(pos.y, pos.z, 0, 1);
        }
        else if(tNorm.y>tNorm.x && tNorm.y>tNorm.z)
        {
            gl_Position = vec4(pos.x, pos.z, 0, 1);
        }
        else
        {
            gl_Position = vec4(pos.x, pos.y, 0, 1);
        }

        EmitVertex();
    }
    EndPrimitive();
}