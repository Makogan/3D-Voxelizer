//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Implementation of major functions of the Helios library
 *
 * @file Helios.cpp
 * @author Camilo Talero
 * @date 2018-04-19
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include files                                    *
 *                                                                                      */
//========================================================================================
#include "Helios.hpp"
#include "System-Libraries.hpp"

using namespace std;
namespace Helios {
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                            Helios Functions Implementations                          *
 *                                                                                      */
//========================================================================================

//Initialize helios
bool HeliosInit()
{
    //Enable debugging messages
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    //TODO: make this NVIDIA specific though a macro
    glDebugMessageCallback((GLDEBUGPROC)Helios::errorCallback, NULL);
    GLuint bind_notification = 0x20071, copy_warning = 0x20072;
    glDebugMessageControl(GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_OTHER,
        GL_DONT_CARE, 1, &bind_notification, GL_FALSE);
    glDebugMessageControl(GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_PERFORMANCE,
        GL_DONT_CARE, 1, &copy_warning, GL_FALSE);

    Texture::assertVAO();

    return true;
}

}//Helios namespace closing bracket
//########################################################################################