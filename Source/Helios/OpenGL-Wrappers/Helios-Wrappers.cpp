//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Implementation of OpenGL wrapping classes for the Helios Library
 *
 * @file Helios-Wrappers.cpp
 * @author Camilo Talero
 * @date 2018-04-15
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================

#include "Helios-Wrappers.hpp"
#include "Helping-Functions.hpp"
#include "Helios/System-Libraries.hpp"

using namespace std;
using namespace glm;
//########################################################################################

namespace Helios{
//========================================================================================
/*                                                                                      *
 *                                     Shader Class                                     *
 *                                                                                      */
//========================================================================================

//──── Static Functions ──────────────────────────────────────────────────────────────────

//Find the type of the shader
GLenum Shader::getFileShaderType(string file_path)
{
    //Set the entire string to lower case
    std::string lname = file_path;
    std::transform(lname.begin(), lname.end(), lname.begin(), ::tolower);

    //Look for a substring in the file to identify the shader type
    if(lname.find("vertex")!=string::npos)
        return GL_VERTEX_SHADER;
    else if(lname.find("tessc")!=string::npos)
        return GL_TESS_CONTROL_SHADER;
    else if(lname.find("tesse")!=string::npos)
        return GL_TESS_EVALUATION_SHADER;
    else if(lname.find("geometry")!=string::npos)
        return GL_GEOMETRY_SHADER;
    else if(lname.find("fragment")!=string::npos)
        return GL_FRAGMENT_SHADER;
    else if(lname.find("compute")!=string::npos)
        return GL_COMPUTE_SHADER;
    else
        return 0;
}

//──── Constructors and Destructors ──────────────────────────────────────────────────────

Shader::Shader(string file_path)
{
    //Ensure file is correct
    if(!check_file(file_path))
        return;

    //Get the shader string into RAM
    string source;
	ifstream input(file_path.c_str());
	copy(istreambuf_iterator<char>(input),
		istreambuf_iterator<char>(),
		back_inserter(source));
	input.close();

    const GLchar* s_ptr = source.c_str();//get raw c string (char array)

    shaderID = glCreateShader(type);//create shader on the GPU
    //Name the object
    glObjectLabel(GL_SHADER, shaderID, file_path.length(), file_path.c_str());
	glShaderSource(shaderID, 1, &s_ptr, NULL);//set shader program source

	glCompileShader(shaderID);

    //verify compilation
	GLint status;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);
	if(status!=GL_TRUE)
	{
		cerr << "Shader compilation error. Could not compile: "
		    << file_path << "\nShader type: "
		    << shaderEnumToString(type)
		    <<endl;

        //Get the shader string from the GPU
		GLint length;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &length);

        //Get the error message associated with the compilation error
		string log(length, ' ');
		glGetShaderInfoLog(shaderID, log.length(), &length, &log[0]);

		cerr<< endl << source <<endl;
		cerr << endl << log <<endl;

        Log::record_log(string(80,'!'));
        Log::record_log("\nShader"+file_path+":\n\n" + source + "\n\n");
        Log::record_log(log+"\n");
        Log::record_log("Shader " + file_path + " failed to compile\n");
        Log::record_log(string(80, '!'));

        //Shader is now useless, delete
        glDeleteShader(shaderID);

		exit(EXIT_FAILURE);
	}
}

//Destructor
Shader::~Shader()
{
    glDeleteShader(shaderID);
}

//──── Private Methods ───────────────────────────────────────────────────────────────────

//Verify that the file exists and that its name identifies it as a valid GLSL shader
bool Shader::check_file(std::string file_path)
{
    //Start reading from the input file
    std::ifstream infile(file_path);
    //Error check and output error message if needed
    if(!infile.good())
    {
        cerr << "Problem with file " + file_path << endl;
        cerr << "It could be that the file is corrupt or it does not exist" << endl;

        Log::record_log(string(80,'-'));
        Log::record_log("Error:\n\tShader not created due to error with file: " + file_path);
        Log::record_log(string(80,'-'));

        exit(EXIT_FAILURE);
    }
    //Get the shader type
    type = Shader::getFileShaderType(file_path);

    if(!type)
    {
        cerr << "Shader File cannot be identified. Please make sure the file name "
            "contains one of:\nvertex, tesse, tessc, geometry, "
            "fragment, compute\n (Not case sensitive)" << endl;
        cerr << "File Path: " + file_path  << endl;

        Log::record_log(string(80,'-'));
        Log::record_log("\tCould not identify shader file: " + file_path);
        Log::record_log(string(80,'-'));

        exit(EXIT_FAILURE);
    }

    //return true upon a good file
    return true;
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                 Shading_Program Class                                *
 *                                                                                      */
//========================================================================================

/**
 * @brief Check that a shading program was successfully compiled
 *
 * @param programID The program we are checking
 * @param f_names A list of the file paths to the shaders used to create the program
*/
void static verify_linking(GLuint programID, vector<string> f_names)
{
    GLint isLinked = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE) //Check that linking was successful
	{
        string file_list = "";
        for(string name:f_names)
        file_list += name + "\n";
		GLint maxLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);
		// The maxLength includes the NULL character
		string infoLog(maxLength, ' ');
		glGetProgramInfoLog(programID, maxLength, &maxLength, &infoLog[0]);
        //Record problem
		cerr << endl << infoLog <<endl;
        Log::record_log(string(80,'!'));
        Log::record_log("\nLinking error:\n" + infoLog);
        Log::record_log("Shader Program failed to link");
        Log::record_log("\nSource files:"
              "\n" + file_list
            );
        Log::record_log(string(80, '!'));

        // The program is useless now. So delete it.
		glDeleteProgram(programID);

		exit(EXIT_FAILURE);
	}
}
//Initialize the Shading program object
Shading_Program::Shading_Program(int f_num, ...)
{
    //Storage for the information passed to the function
    vector<Shader*> shaders;
    vector<string> f_names;

    va_list file_names;
    va_start(file_names, f_num);

    //Compile the shaders
    for(int i=0; i<f_num; i++)
    {
        string name(va_arg(file_names, char*));
        f_names.push_back(name);
        shaders.push_back(new Shader(name));
    }

    //Initialize and create the rendering program
	programID = glCreateProgram();
    string name = string(basename((char*) f_names[0].c_str()));
    size_t lastindex = name.find_last_of("-");
    name = name.substr(0, lastindex);
    glObjectLabel(GL_PROGRAM, programID, -1, ("\""+name+"\"").c_str());

    //Attach the shaders to the program
    for(int c_shader=0; c_shader < shaders.size(); c_shader++)
        shaders[c_shader]->attachTo(programID);
    //Attempt to link the program
    glLinkProgram(programID);
    verify_linking(programID, f_names);
    //Delete the shaders as they are no longer needed
    for(int c_shader=0; c_shader < shaders.size(); c_shader++)
    {
        shaders[c_shader]->detachFrom(programID);
        delete(shaders[c_shader]);
    }
    va_end(file_names);
}

Shading_Program::~Shading_Program()
{
    glDeleteProgram(programID);
}

//──── Other Functions ───────────────────────────────────────────────────────────────────

        //Retrieve uniform location
        GLint Shading_Program::get_uniform_location(std::string name)
        {
            //Get the current active program
            use();
            //Get the location of the uniform in the shader
            GLint loc = glGetUniformLocation(programID, name.c_str());
            //Error handling
            if(loc == GL_INVALID_VALUE || loc==GL_INVALID_OPERATION || loc==-1)
            {
                std::cerr << "Error returned when trying to find uniform " <<
                    "\"" + name + "\"" << std::endl;

                if(loc == -1)
                {
                    char label_buffer[100];
                    GLsizei buff_size;
                    glGetObjectLabel(GL_PROGRAM, programID, 100, &buff_size, label_buffer);

                    Log::record_log(
                        std::string(80, '!') + "\nFailed to find uniform: " + name + "\n"
                        + "No such uniform exists in program: " + std::string(label_buffer)
                        + "\n" + std::string(80, '!')
                    );
                }
                else
                {
                    Log::record_log(
                        std::string(80, '!') +
                        "Problem when requesting uniform: \"" + name + "\"\n" +
                        "Check that there is an active program and that it has been"
                        "correctly Initialized" +
                        std::string(80, '!')
                    );
                }
               //exit(EXIT_FAILURE);
            }

            return loc;
        }
//########################################################################################

}//Closing bracket of Helios namespace