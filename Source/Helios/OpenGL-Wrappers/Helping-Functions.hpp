//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Header declaring some commonly used functions
 * 
 * @file Helping-Functions.hpp
 * @author Camilo Talero
 * @date 2018-07-05
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

//========================================================================================
/*                                                                                      *
 *                                    Helping Methods                                   *
 *                                                                                      */
//========================================================================================
/**
 * @brief Set the data for a VBO
 *
 * @tparam T The type of the data with which to fill the buffer
 * @param buffer The OpenGL ID of the buffer
 * @param data Vector containing the data to fill the buffer with
*/
template <class T>
void inline static set_data_buffer(GLuint buffer, std::vector<T> &data, std::string name)
{
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glObjectLabel(GL_BUFFER, buffer, -1, name.c_str());
    glBufferData(GL_ARRAY_BUFFER, sizeof(T)*data.size(), data.data(), GL_STATIC_DRAW);
}
/**
 * @brief Set the indices for indexed rendering in a VBO
 *
 * @tparam T The type if the index data  (should be uint, left as general just in case)
 * @param buffer The OpenGL ID of the buffer
 * @param data Vector containing the data to fill the buffer with
*/
template <class T>
void inline static set_indices_buffer(GLuint buffer, std::vector<T> &data, std::string name)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
    glObjectLabel(GL_BUFFER, buffer, -1, name.c_str());
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(T)*data.size(), data.data(),
        GL_STATIC_DRAW);
}
/**
 * @brief Set the attribute locations for a shader
 *
 * Each of the arrays should have the same number of elements. Each parameter is a list
 * of values to be passed top the corresponding parameter for
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glVertexAttribFormat.xhtml.
 * Each index in one array represents one paremeter.
 *
 * @param locations Array of attribute locations
 * @param element_size Number of elements (counted as 4 bytes per element) per vertex instance
 * @param normalize Should the values be normalized
 * @param element_distance Distance between consecutive elements in the array
*/
void inline static set_attribute_locations(std::vector<GLuint> &locations,
    std::vector<GLint> &element_size, std::vector<GLboolean> &normalize,
    std::vector<GLuint> &element_distance)
{
    //check that array sizes match one another
    if(locations.size()!=element_size.size() || locations.size()!=normalize.size() ||
        locations.size()!=element_distance.size())
    {
        std::cerr << "Un-matching array sizes for set_attribute_locations()" << std::endl;
        Log::record_log(std::string(80, '!') +
            "\nUn-matching array sizes for set_attribute_locations()\n" +
            std::string(80, '!'));
    }
    //Specify layout format for each target location (the way the data is to be read)
    //This needs to match the layout declarations in the shaders
    int buffer_index = 0;
    for(uint i=0; i<locations.size(); i++)
    {
        glEnableVertexAttribArray(locations[i]);
        glVertexAttribFormat(locations[i], element_size[i],
            GL_FLOAT, normalize[i], element_distance[i]);
        glVertexAttribBinding(locations[i], buffer_index++);
    }
}
/**
 * @brief Extract a base name from a file path
 *
 * @param file_path
 * @return string
*/
std::string inline static extract_name(std::string file_path)
{
    std::string name = std::string(basename((char*) file_path.c_str()));
    size_t lastindex = name.find_last_of(".");
    return name.substr(0, lastindex);
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                   Printing Methods                                   *
 *                                                                                      */
//========================================================================================
/**
 * @brief Print a vec2
 *
 * @param os output stream
 * @param v vector
 * @return std::ostream& string representing the vector
*/
std::ostream inline static &operator<<(std::ostream &os, glm::vec2 &v)
{
    return os << "(" << v.x <<", "<< v.y << ")";
}
/**
 * @brief Print a vec3
 *
 * @param os output stream
 * @param v vector
 * @return std::ostream& string representing the vector
*/
std::ostream inline static &operator<<(std::ostream &os, glm::vec3 &v)
{
    return os << "(" << v.x <<", "<< v.y << ", "<< v.z <<")";
}
/**
 * @brief Print an ivec3
 *
 * @param os output stream
 * @param v vector
 * @return std::ostream& string representing the vector
*/
std::ostream inline static &operator<<(std::ostream &os, glm::ivec3 &v)
{
    return os << "(" << v.x <<", "<< v.y << ", "<< v.z <<")";
}
/**
 * @brief Print a vec4
 *
 * @param os output stream
 * @param v vector
 * @return std::ostream& string representing the vector
*/
std::ostream inline static &operator<<(std::ostream &os, glm::vec4 &v)
{
    return os << "(" << v.x <<", "<< v.y << ", "<< v.z << ", " << v.w << ")";
}
/**
 * @brief Print a generic vector
 *
 * @tparam T A generic type, assumed to be printable
 * @param os The ouput stream
 * @param v the generic vector to be printed
 * @return std::ostream& the string representing the object
*/
template <class T>
std::ostream inline static &operator<<(std::ostream &os, std::vector<T> &v)
{
    int counter=0;
	os << "<";
    for(float t: v)
    {
        if(counter<=v.size())
            os << t << ", ";
        else
            os << t << ">";
        counter++;
    }
    return os;
}

//########################################################################################