//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Implementation fo the geometry wrappers
 *
 * @file Geometry-Wrappers.cpp
 * @author Camilo Talero
 * @date 2018-07-05
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================

#include "Geometry-Wrappers.hpp"
#include "Helios-Wrappers.hpp"
#include "Helios/System-Libraries.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"

using namespace std;
using namespace glm;
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                    Helping Methods                                   *
 *                                                                                      */
//========================================================================================
/**
 * @brief Get the section contained in between the first 2 occurrences of a pattern
 *
 * The returned string will be include the first occurrence of the pattern and exclude the
 * second.
 *
 * @param message Full message. Upon return will contain the part of the message after
 *      the section contained in between the pattern
 * @param pattern Pattern used to delimit the section
 * @param holder The section in between the first 2 occurrences of the pattern
*/
void inline static get_section(string &message, string pattern, string &holder)
{
    //Extract full mesage from first occurrence of pattern
    size_t index = message.find(pattern);
    if(index==string::npos){holder = ""; return;}
    message = message.substr(index, message.size());
    //Extract full message after first occurrence of pattern
    string temp = message.substr(pattern.size(), message.size());
    //Find next occurrence of pattern
    index = temp.find(pattern);
    if(index==string::npos){index=temp.size();}
    //Copy current message from the start up to the second occurrence of the pattern
    holder = message.substr(0, index+pattern.size());
    //Copy the message from the seccond occurrence of the pattern to the end
    message = message.substr(holder.size(), message.size());
}
//Extract material names into an array and change their texture paths to the corresponding
//offset value or -1 if the texture does not exist
vector<string> inline static organize_materials(vector<Helios::Material> material_list)
{
    vector<string> diffuse_list;
    for(int m_index=0; m_index<material_list.size(); m_index++)
    {
        string path = material_list[m_index].diffuse_map;
        diffuse_list.push_back(material_list[m_index].diffuse_map);
        material_list[m_index].diffuse_map = path == ""? -1: m_index;
    }

    return diffuse_list;
}
//Find the index of a a material by name in the provided array, or -1 if it's not found
int inline static get_mtl_index(vector<Helios::Material> mtl_list, string mtl)
{
    for(uint i=0; i<mtl_list.size(); i++)
    {
        if(mtl == mtl_list[i].name)
            return i;
    }

    return -1;
}
//Extract material information from the mtl file associated with an obj file
vector<string> parse_materials(string file_path, vector<Helios::Material> &material_list)
{
    ifstream wfile;

    wfile.open(file_path);
    //error check
    if (!wfile)
    {
        cerr << "Unable to open file " << file_path << endl;
        Log::record_log(
            string(80, '!') +
            "Error with file " + file_path + " when calling parse_wavefront" +
            string(80, '!')
            );
        //call system to stop
        exit(EXIT_FAILURE);
    }
    //Put the entire string into a string
    string file_string((istreambuf_iterator<char>(wfile)), istreambuf_iterator<char>());
    string mtl="";
    //Iterate through each material
    while(file_string != "")
    {
        //Extract current material
        get_section(file_string, "newmtl", mtl);
        //Construct a material from the available information
        material_list.push_back(Helios::Material(mtl));
    }

    wfile.close();
    //Finnish the material information and associate to each one an offset in an array
    vector<string> file_list = organize_materials(material_list);
    //Build a texture array in the GPU that mirrors the order of the materials in the list
    return file_list;
}

//########################################################################################

namespace Helios{
//========================================================================================
/*                                                                                      *
 *                                     Texture CLass                                    *
 *                                                                                      */
//========================================================================================
GLuint Texture::T_VAO=0;
//Main constructor
Texture::Texture(string file_path, GLuint t_target)
{
    //Change the coordinate system of the image
    stbi_set_flip_vertically_on_load(true);
    int numComponents;
    //Load the pixel data of the image
    void *data = stbi_load(file_path.c_str(), &width, &height, &numComponents, 0);
    if (data == nullptr)//Error check
    {
        cerr << "Error when loading texture from file: " + file_path << endl;

        Log::record_log(
            string(80, '!') +
            "\nError when loading texture from file: " + file_path + "\n" +
            string(80, '!')
        );
        exit(EXIT_FAILURE);
    }
    //Create the texture OpenGL object
    target = t_target;
    glGenTextures(1, &textureID);
    glBindTexture(target, textureID);
    //Name the texture
    glObjectLabel(GL_TEXTURE, textureID, -1,
        ("\"" + extract_name(file_path) +"\"").c_str());
    //Set the color format
    color_format = numComponents == 3 ? GL_RGB : GL_RGBA;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(target, 0, color_format, width, height, 0,
        color_format, GL_UNSIGNED_BYTE, data);
    //Set the texture parameters of the image
   	glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    //free the memory
    stbi_image_free(data);
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                 Texture2D_Array Class                                *
 *                                                                                      */
//========================================================================================
//Construct a texture 2D array
Texture2D_Array::Texture2D_Array(vector<string> file_names, GLuint t_target)
{
    //Parse files to find largest dimensions
    int m_width=0, m_height=0;
    for(uint image=0; image<file_names.size(); image++)
    {
        if(file_names[image] != "")
        {
            int c_width, c_height, components;
            stbi_info(file_names[image].c_str(), &c_width, &c_height, &components);
            //Set the current largest dimension found
            m_width = glm::max(m_width, c_width);
            m_height = glm::max(m_height, c_height);
        }
    }
    //Create the texture OpenGL object
    target = t_target;
    glGenTextures(1, &textureID);
    glBindTexture(target, textureID);
    //Name the texture
    glObjectLabel(GL_TEXTURE, textureID, -1,
        ("\"" + extract_name(file_names[0]) +"\"").c_str());
    //Allocate storage space for the entire texture array
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, m_width, m_height, file_names.size());
    //Array to store the stretching coefficients
    vector<vec2> stretch_coeffs(file_names.size());
    for(uint file=0; file<file_names.size(); file++)
    {
        if(file_names[file] == "")
            continue;
        //Change the coordinate system of the image
        stbi_set_flip_vertically_on_load(true);
        int numComponents;
        //Load the pixel data of the image
        void *data = stbi_load(file_names[file].c_str(),
            &width, &height, &numComponents, 0);
        if (data == nullptr)//Error check
        {
            cerr << "Error when loading texture from file: " + file_names[file] << endl;

            Log::record_log(
                string(80, '!') +
                "\nError when loading texture from file: " + file_names[file] + "\n" +
                string(80, '!')
            );
            glDeleteTextures(1, &textureID);
            exit(EXIT_FAILURE);
        }
        //Set the color format
        color_format = numComponents == 3 ? GL_RGB : GL_RGBA;
        //Set the unpack alignment to 1 byte
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        //Store the image into a layer of the texture array
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, file, width, height, 1,
            color_format, GL_UNSIGNED_BYTE, data);
        //Store the stretch fo this image relative to the maximum dimensions of all images
        stretch_coeffs[file] = (
            vec2((float)width/(float)m_width, (float)height/(float)m_height));
        //free the memory
        stbi_image_free(data);
    }
    //Set the texture parameters of the image
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    //Store the information in an SSBO
    relative_stretches = new Storage_Buffer(stretch_coeffs.size()*sizeof(vec2));
    relative_stretches->set_data(stretch_coeffs);
    relative_stretches->name("Texture array stretching coefficients buffer");
}
//Bind the texture to a binding point
void Texture2D_Array::bind_texture(GLuint texture_unit)
{
    Texture::bind_texture(texture_unit);
    relative_stretches->bind_buffer(0);
}
//Bind a texture and it's associated stretching buffer to binding points
void Texture2D_Array::bind_texture(GLuint texture_unit, GLuint buffer_binding_point)
{
    Texture::bind_texture(texture_unit);
    relative_stretches->bind_buffer(buffer_binding_point);
}
//Destroy the texture array
Texture2D_Array::~Texture2D_Array()
{
    glDeleteTextures(1, &textureID);
    if(relative_stretches != NULL)
        delete(relative_stretches);
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                     Image3D Class                                    *
 *                                                                                      */
//========================================================================================
//Constructor
Image3D::Image3D(int w, int h, int d)
{
    //Initialize texture dimensions
    width = w;
    height = h;
    depth = d;
    //set the texture rendering target, always GL_TEXTURE3D for 3D images
    target = GL_TEXTURE_3D;
    //Create the texture
    glGenTextures(1, &textureID);
    glBindTexture(target, textureID);
    glObjectLabel(GL_TEXTURE, textureID, -1, "\"3D Texture\"");
    //Set the texture sampling parameters
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //Allocate VRAM storage for the texture
    int big = glm::max(w,h);
    big = glm::max(big, d);
    glTexStorage3D(GL_TEXTURE_3D, ceil(std::log2(big)), GL_RGBA8, width, height, depth);
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                    Material Class                                    *
 *                                                                                      */
//========================================================================================

//macro to get 3 value sof a file into a vec3
#define GET_VEC(destination) \
    string num;\
    float x,y,z;\
    l_stream >> num; x = stof(num);\
    l_stream >> num; y = stof(num);\
    l_stream >> num; z = stof(num);\
    destination = vec3(x,y,z);\
    continue;

Material::Material(string mtl_info)
{
    stringstream iss(mtl_info);
    string line;
    //Iterate through the string line by line
    while(getline(iss, line))
    {
        //Get the first word of the line
        stringstream l_stream(line);
        string start;
        l_stream >> start;
        //Initialize the name
        if(start=="newmtl")
        {
            l_stream >> name;
            continue;
        }
        //Initialize the ambient reflectiveness
        if(start=="Ka")
        {
            GET_VEC(ambient_ref);
        }
        //Initialize the diffuse reflectiveness
        if(start=="Kd")
        {
            GET_VEC(diffuse_ref);
        }
        //Initialize the specular reflectiveness
        if(start=="Ks")
        {
            GET_VEC(spec_ref);
        }
        //Initialize emissiveness
        if(start=="Ke")
        {
            GET_VEC(emissiveness);
        }
        //Initialize the transmission filter
        if(start=="Tf")
        {
            GET_VEC(transmission_filter);
        }
        //Initialize the illumination model identifier
        if(start=="illum")
        {
            string num;
            l_stream >> num;
            illum_model = stoi(num);
            continue;
        }
        //Initialize the background dissolution coefficient
        if(start=="d")
        {
            string num;
            l_stream >> num;
            dissolve = stof(num);
            continue;
        }
        //Initialize the specular exponent coefficient
        if(start=="Ns")
        {
            string num;
            l_stream >> num;
            spec_exponent = stof(num);
            continue;
        }
        //Initialize the refraction index
        if(start=="Ni")
        {
            string num;
            l_stream >> num;
            refraction_index = stof(num);
            continue;
        }
        //Initialize the ambient color map
        if(start=="map_Ka")
        {
            l_stream >> ambient_map;
            continue;
        }
        //Initialize the diffuse color map
        if(start=="map_Kd")
        {
            l_stream >> diffuse_map;
            continue;
        }
        //Initialize the specular color map
        if(start=="map_Ks")
        {
            l_stream >> specular_map;
            continue;
        }
        //Initialize the Bump map
        if(start=="map_Bump")
        {
            l_stream >> bump_map;
            continue;
        }
    }

    /*cout << name << endl;
    cout << "Ka " << ambient_ref << endl;
    cout << "Kd " << diffuse_ref << endl;
    cout << "Ks " << spec_ref << endl;
    cout << "Ke " << emissiveness << endl;
    cout << "Tf " << transmission_filter << endl;
    cout << "illum " << illum_model << endl;
    cout << "d " << dissolve << endl;
    cout << "Ns " << spec_exponent << endl;
    cout << "Ni " << refraction_index << endl;
    cout << "map_Ka " << *(ambient_map.path) << endl;
    cout << "map_Kd " << *(diffuse_map.path) << endl;
    cout << "map_Ks " << *(specular_map.path) << endl;
    cout << "map_Bump " << *(bump_map.path) << endl;*/
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                      Mesh Class                                      *
 *                                                                                      */
//========================================================================================

//Enumerators used to index through the buffers[] array
enum {MESH_VERTEX_BUFFER=0, MESH_NORMAL_BUFFER, MESH_UV_BUFFER, MESH_INDICES_BUFFER};

//Default mesh constructor, for testing only
Mesh::Mesh()
{
    //Create a triangle for illustration purposes
    vertices = {glm::vec3(-1,-1,0), glm::vec3(1,-1,0), glm::vec3(0,1,0)};
    normals = {glm::vec3(-1,-1,0), glm::vec3(1,-1,0), glm::vec3(0,1,0)};
    uvs = {vec3(0,0,0), vec3(1,0,0), vec3 (0,1,0)};
    indices = {0,1,2};

    //Initialize VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glObjectLabel(GL_VERTEX_ARRAY, VAO, -1, "\"Generic mesh VAO\"");

    //Initialize buffers and fill them with data
    glGenBuffers(4, buffers);
    set_data_buffer(buffers[MESH_VERTEX_BUFFER], vertices,
        "\"Default Mesh Vertex Buffer\"");
    set_data_buffer(buffers[MESH_NORMAL_BUFFER], normals,
        "\"Default Mesh Normal Buffer\"");
    set_data_buffer(buffers[MESH_UV_BUFFER], uvs,
        "\"Default Mesh UV Buffer\"");
    set_indices_buffer(buffers[MESH_INDICES_BUFFER], indices,
        "\"Default Mesh Index Buffer\"");

    //Set attribute location information
    vector<GLuint> locs = {0,1,2};  // attribute locations 0,1,2
    vector<GLint> sizes = {3,3,3};  // element sizes of 3,3,2 bytes (vec3, vec3, vec3)
    vector<GLboolean> normalize = {false, true, false}; //Should the element be normalized
    vector<GLuint> distance = {0,0,0}; //Distance between elements of the buffer
    set_attribute_locations(locs, sizes, normalize, distance);
}
Mesh::Mesh(string file_path) : Mesh::Mesh(file_path, {}){}
//Construct a mesh from a file
Mesh::Mesh(string file_path, vector<Helios::Material> material_list)
{
    //Load an object from a wavefront file
    load_from_obj(file_path, material_list);

    //Extract base file name
    string name = extract_name(file_path);

    //Initialize VAO
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glObjectLabel(GL_VERTEX_ARRAY, VAO, -1, string("\"" + name + " mesh VAO\"").c_str());

    //Initialize buffers and fill them with data
    glGenBuffers(4, buffers);
    set_data_buffer(buffers[MESH_VERTEX_BUFFER], vertices,
        "\"" + name + " mesh vertex buffer\"");
    set_data_buffer(buffers[MESH_NORMAL_BUFFER], normals,
        "\"" + name + " mesh normal buffer\"");
    set_data_buffer(buffers[MESH_UV_BUFFER], uvs,
        "\"" + name + " mesh uv buffer\"");
    set_indices_buffer(buffers[MESH_INDICES_BUFFER], indices,
        "\"" + name + " mesh index buffer\"");

    //Set attribute location information
    vector<GLuint> locs = {0,1,2};  // attribute locations 0,1,2
    vector<GLint> sizes = {3,3,3};  // element sizes of 3,3,3 floats (vec3, vec3, vec3)
    vector<GLboolean> normalize = {false, true, false}; //Should the element be normalized
    vector<GLuint> distance = {0,0,0}; //Distance between elements of the buffer
    set_attribute_locations(locs, sizes, normalize, distance);
}
// Mesh destructor
Mesh::~Mesh()
{
    glDeleteBuffers(4, buffers);
}
//overlaoded assignment operator
Mesh& Mesh::operator=(const Mesh& o_mesh)
{
    glGenVertexArrays(1,&VAO);

    vertices = o_mesh.vertices;
    normals = o_mesh.normals;
    uvs = o_mesh.uvs;
    indices = o_mesh.indices;

    glGenBuffers(4, buffers);
    set_data_buffer(buffers[MESH_VERTEX_BUFFER], vertices,
        "\"Mesh Vertex Buffer\"");
    set_data_buffer(buffers[MESH_NORMAL_BUFFER], normals,
        "\"Mesh Normal Buffer\"");
    set_data_buffer(buffers[MESH_UV_BUFFER], uvs,
        "\"Mesh UV Buffer\"");
    set_indices_buffer(buffers[MESH_INDICES_BUFFER], indices,
        "\"Mesh Index Buffer\"");
}

//Draw the mesh
void Mesh::draw()
{
    //Bind the buffers and draw their contents
    GLintptr offsets[] = {0,0,0};
    int strides[] = {sizeof(vec3),sizeof(vec3), sizeof(vec3)};
    glBindVertexBuffers(0, 3, buffers, offsets, strides);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}

void Mesh::load_from_obj(string file_path){ load_from_obj(file_path, {}); }
//Load mesh from .obj file
void Mesh::load_from_obj(string file_path, vector<Helios::Material> mtl_list)
{
    vector<vec3> vs, ns;
    vector<vec3> ts;

    ifstream wfile;
    wfile.open(file_path);
    //error check
    if (!wfile)
    {
        cerr << "Unable to open file " << file_path << " when creating mesh" <<endl;
        Log::record_log(
            string(80, '!') +
            "Error with file " + file_path + " when crating mesh" +
            string(80, '!')
            );
        exit(EXIT_FAILURE);   // call system to stop
    }
    string line;
    while(getline(wfile, line))
    {
        //Create string stream and read the first word
        stringstream ss(line);
        string word;
        ss >> word;
        //If the first word in the line is 'v'
        //then this is a vertex information line
        if(word == "v")
        {
            vector<double> coords;
            //convert and store info to temporary vertex array
            while(ss>>word)
                coords.push_back(stof(word));
            vs.push_back(vec3(coords[0], coords[1], coords[2])*1.0f);
        }
        //If the first word in the line is 'vt' this is
        //a texture coordinate information line
        else if(word == "vt")
        {
           vector<double> coords;
            //convert and store info to temporary uvs array
            while(ss>>word)
                coords.push_back(stof(word));
            ts.push_back(vec3(coords[0], coords[1],0));
        }
        //If the first word in the line is 'vn' this is
        //a normals information line
        else if(word == "vn")
        {
            vector<double> coords;
            //convert and store info to temporary vertex array
            while(ss>>word)
                coords.push_back(stof(word));
            ns.push_back(vec3(coords[0], coords[1], coords[2]));
        }
    }
    //reset stream to the beggining of the file
    wfile.clear();
    wfile.seekg(0, ios::beg);
    //once again read every line
    int mtl_index=0;
    while(getline(wfile, line))
    {
        //once again read word by word
        stringstream ss(line);
        string word;
        ss >> word;
        if(word=="usemtl")
        {
            string mtl;
            ss >> mtl;
            mtl_index=get_mtl_index(mtl_list, mtl);
        }
        //if the first word is 'f' this is a face information line
        if(word == "f")
        {
            //process the word, there should be 3 consecutive such words only
            while(ss>>word)
            {
                //split the string
                replace(word.begin(), word.end(), '/', ' ');

                //Store index information of the vertex/texture/normal relationship
                stringstream ws(word);
                vector<int> indices;
                string number;
                for(int i=0; i<3; i++)
                {
                    ws >> number;
                    indices.push_back(stoi(number)-1);
                }

                //Push associated vertex and normal information into the return arrays
                //Now each vertex is associated with the correct normal.
                vertices.push_back(vs[indices[0]]);
                normals.push_back(ns[indices[2]]);
                ts[indices[1]].z = mtl_index;
                //Same as above for texture coordinates
                uvs.push_back(ts[indices[1]]);

            }
        }
    }
    //Close the file
    wfile.close();
}
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                 Object Template Class                                *
 *                                                                                      */
//========================================================================================
//TODO: document the class
Object_Template::Object_Template(string file)
{
    size_t ext_index = file.find_last_of(".");
    string mat_file = file.substr(0,ext_index)+".mtl";

    vector<string> files = parse_materials(mat_file, materials);
    Texture2D_Array temp(files, GL_TEXTURE_2D_ARRAY);
    diffuse_maps(temp);
    mesh = Mesh(file, materials);
}

void Object_Template::bind_default()
{
    diffuse_maps.bind_texture(0);
}

void Object_Template::bind_texture_default(GLuint bbpoint)
{
    diffuse_maps.bind_texture(0, bbpoint);
}

void Object_Template::bind_texture_default(GLuint t_unit, GLuint bbpoint)
{
    diffuse_maps.bind_texture(t_unit, bbpoint);
}

void Object_Template::draw()
{
    mesh.draw();
}
//########################################################################################
}//Close Helios namespace