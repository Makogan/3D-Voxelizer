//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * @brief Header declaration of the OpenGL wrapping classes of the Helios library
 *
 * @file Helios-wrappers.hpp
 * @author Camilo Talero
 * @date 2018-04-15
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================
#pragma once

#include "Helios/System-Libraries.hpp"
#include "Debugging.hpp"
//########################################################################################

namespace Helios{
//========================================================================================
/*                                                                                      *
 *                                 Forward Declarations                                 *
 *                                                                                      */
//========================================================================================
class Image3D;
class Texture;
class Mesh;
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                  Class Declarations                                  *
 *                                                                                      */
//========================================================================================

/**
 * @ingroup Helios
 *
 * @brief Class to wrap GLSL shaders.
 *
*/
class Shader
{
//──── Private Members ───────────────────────────────────────────────────────────────────

    private:
        GLuint shaderID; //!< The OpenGL generated identifier
        GLenum type;    //!< The shader type (e.g GL_VERTEX_SHADER)

        /**
         * @brief Help initialize a shader by first error checking a file
         *
         * @param file_path Path to the shader source file
         * @return true If file was successfully found
         * @return false If file was nto found or did not contain the shader type in
         *         its name
        */
        bool check_file(std::string file_path);

    public:

//──── Static Members ────────────────────────────────────────────────────────────────────
    /**
     * @brief Get the shader enumerator based on the name of a file
     *
     * It searches through the file name for one of:
     *  vertex, tessc, tesse, geometry, fragment, compute (not case sensitive)
     *
     * @param file_path The path tot he file whose enumerator we want to get
     * @return GLenum 0 on error or the appropriate GLenum of the shader type
    */
    static GLenum getFileShaderType(std::string file_path);

//──── Constructors and Destructors ──────────────────────────────────────────────────────

        /**
         * @brief Construct a new Shader object
         *
         * The valid shader names are anything that contains one of the following
         * substrings (not case sensitive):
         *
         * - Vertex
         * - Tessc
         * - Tesse
         * - Geometry
         * - Fragment
         * - Compute
         *
         * @param file_path Path to the shader source file (shader source file must
         * contain it's shader type in it's name)
        */
        Shader(std::string file_path);

        /**
         * @brief Destroy the Shader object
         *
        */
        ~Shader();

//──── Getters and Setters ───────────────────────────────────────────────────────────────

        ///@{
        /**
         * @brief Get the associated variable
         *
        */
        GLuint inline getShaderID(){return shaderID;}
        GLenum inline getType(){return type;}
        ///@}

//──── Other Functions ───────────────────────────────────────────────────────────────────

        /**
         * @brief Attach the shader to a program
         *
         * @param program The program we are attaching to's ID
        */
        void inline attachTo(GLuint program){glAttachShader(program, shaderID);}
        /**
         * @brief Detach The shader from a program
         *
         * @param program The program we are detaching from's ID
        */
        void inline detachFrom(GLuint program){glDetachShader(program, shaderID);}
};
/**
 * @brief Class to wrap OpenGL shading programs
 *
*/
class Shading_Program
{
//──── Private Members ───────────────────────────────────────────────────────────────────

    private:
        GLuint programID=0;   //!< OpenGL generated identifier

    public:

//──── Constructors and Destructors ──────────────────────────────────────────────────────

        /**
         * @brief Construct a new Shading_Program object
         *
         * This should be an integer followed by a matching number of file paths identifying
         * each shader used in the program. Undefined behaviour occurs if \a f_num \a is
         * not exactly the number of shading programs that follows and there is no error
         * checking for it. The only valid arguments after f_num are strings, anything
         * else is undefined behaviour, do not give a wrong number of parameters. Always
         * use the complete relative or ful  path to the shader files (1-6 parameters).
         *
         * Example use:
         *      v = new Helios::Shading_Program(
         *          2,
         *          "Shaders/Basic-Vertex.glsl",
         *          "Shaders/Basic-Fragment.glsl");
         *
         * @param f_num The number of files passed as an argument to the function
         * @param ... The file paths to each shader
        */
        Shading_Program(int f_num, ...);
        /**
         * @brief Destroy the Shading_Program object
         *
        */
        ~Shading_Program();

//──── Setters and Getters ───────────────────────────────────────────────────────────────
        /**
         * @brief Get the OpenGL ProgramID
         *
         * @return GLuint
        */
        GLuint inline getProgramID(){return programID;}

//──── Other Functions ───────────────────────────────────────────────────────────────────

        /**
         * @brief use the current program
         *
        */
        void inline use(){glUseProgram(programID);}
        /**
         * @brief Set the program's OpenGL label
         *
         * @param name The new label of the program
        */
        void inline set_program_name(std::string name)
        {glObjectLabel(GL_PROGRAM, programID, -1, name.c_str());}
        /**
         * @brief Bind a texture to a uniform of the current program
         *
         * @param texture
         * @param uniform_label
        */
        void bind_texture(Texture texture, std::string uniform_label);

//──── Uniform Functions ─────────────────────────────────────────────────────────────────

        /**
         * @brief Get the location of the uniform labeled <name> in the current program
         *
         * @param name The string representing the uniform to be found
         * @return GLint The location of the uniform in the current shading program
        */
        GLint get_uniform_location(std::string name);
        /**
         * @name Uniform Loading Functions
         *
         * @brief Each of the following functions loads the values described in the first
         * parameter into the uniform labeled "name"
         *
         * @param type the structure containing the info to load
         * @param name string describing the name of the uniform as it appears on the shaders
        */
        ///@{
        void inline load_uniform(glm::mat4 matrix, std::string name)
        {
            GLint loc = get_uniform_location(name);
            glUniformMatrix4fv(loc, 1, GL_FALSE, value_ptr(matrix));
        }
        void inline load_uniform(glm::vec4 vector, std::string name)
        {
            GLint loc = get_uniform_location(name);
            glUniform4fv(loc, 1, (GLfloat*)&(vector));
        }
        void inline load_uniform(glm::vec3 vector, std::string name)
        {
            GLint loc = get_uniform_location(name);
            glUniform3fv(loc, 1, (GLfloat*)&(vector));
        }
        void inline load_uniform(float num, std::string name)
        {
            GLint loc = get_uniform_location(name);
            glUniform1f(loc, num);
        }
        void inline load_uniform(double num, std::string name)
        {
            GLint loc = get_uniform_location(name);
            glUniform1f(loc, num);
        }
        void inline load_uniform(int num, std::string name)
        {
            GLint loc = get_uniform_location(name);
            glUniform1i(loc, num);
        }
        ///@}
};

/**
 * @brief Class to wrap OpenGL SSBOs
 *
*/
class Storage_Buffer
{

//──── Private Members ───────────────────────────────────────────────────────────────────

    private:

        GLuint SSBO;            //!< OpenGL SSBO
        GLint data_size = -1;   //!< Size of current data in the buffer
        GLint max_data = -1;    //!< Maximum capacity of the buffer

//──── Public Members ────────────────────────────────────────────────────────────────────

    public:

//──── Constructors and Destructors ──────────────────────────────────────────────────────

        /**
         * @brief Construct a new Storage_Buffer object
         *
         * @param size The size of the buffer to allocate in bytes
        */
        Storage_Buffer(GLint size)
        {
            // Create the SSBO identifier
            glGenBuffers(1, &SSBO);
            // Name and allocate the buffer storage
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glObjectLabel(GL_BUFFER, SSBO, -1, "\"SSBO\"");
            std::vector<GLbyte> zeros = std::vector<GLbyte>(size);
            glBufferStorage(GL_SHADER_STORAGE_BUFFER, size, zeros.data(),
                GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_DYNAMIC_STORAGE_BIT);
            //Set the maximum buffer capacity
            max_data = size;
        }
        /**
         * @brief Destroy the Storage_Buffer object
         *
        */
        ~Storage_Buffer(){glDeleteBuffers(1, &SSBO);}

//──── Other Methods ─────────────────────────────────────────────────────────────────────

        /**
         * @brief Set the OpenGL string label for the SSBO
         *
         * @param name
        */
        void name(std::string name)
        {
            glObjectLabel(GL_BUFFER, SSBO, -1, name.c_str());
        }
        /**
         * @brief Bind a buffer to a binding point in the context
         *
         * @param binding_point Buffer Binding point (as declared in the shader)
        */
        void inline bind_buffer(GLuint binding_point)
        {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_point, SSBO);
        }
        /**
         * @brief Set the buffer data to an int (only first 8 bytes will be used)
         *
         * @param data The integer's value
        */
        void set_data(GLint data)
        {
            data_size = sizeof(GLint);
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0,
                data_size, &data);
        }
        /**
         * @brief Set the buffer data to an array of floats
         *
         * @param data The data to fill the buffer with
        */
        void set_data(std::vector<GLfloat> &data)
        {
            data_size = sizeof(GLfloat)*data.size();
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0,
                data_size, data.data());
        }
        /**
         * @brief Set the buffer data to an array of vec2's
         *
         * @param data The data to fill the buffer with
        */
        void set_data(std::vector<glm::vec2> &data)
        {
            data_size = sizeof(glm::vec2)*data.size();
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0,
                data_size, data.data());
        }
        /**
         * @brief Set the buffer data through a custom struct
         *
         * For this to work the structure must implement a
         * \a getCompactData(GLint* data_size, GLvoid *data) \a method that will allocate
         * the data contiguously in memory and return the data size in bytes in the first
         * argument and allocate a contiguous memory location representing the data to be
         * sent to VRAM.
         *
         * @param data_struct A generic data structure that implements
         * \a getCompactData(GLint* data_size, GLvoid *data) \a
        */
        template <typename T>
        void set_data(T &data_struct)
        {
            GLvoid *data;
            data_struct.getCompactData(&data_size, data);
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, data_size, data);
        }
        /**
         * @brief Get the first GLint allocated in the buffer
         *
         * @return GLint The first value in the buffer as a GLint
        */
        GLint get_rawGLint()
        {
            GLint retVal;
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, sizeof(GLint), &retVal);
            return retVal;
        }
        /**
         * @brief Get the raw data in the buffer as an array of bytes
         *
         * @return std::vector<GLbyte> the data stored in the buffer
        */
        std::vector<GLbyte> get_raw_data()
        {
            std::vector<GLbyte> holder = std::vector<GLbyte>(data_size);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, data_size, holder.data());
            return holder;
        }
        /**
         * @brief Clear the buffer
         * 
        */
        void clear_data()
        {
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
            float zero=0.0f;
            glClearBufferData(GL_SHADER_STORAGE_BUFFER, GL_R32F, GL_RED, GL_FLOAT, &zero);
            data_size = 0;
        }
};
}//Close helios namespace
//########################################################################################

