//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Header for the Geometry Wrappers
 *
 * @file Geometry-Wrappers.hpp
 * @author Camilo Talero
 * @date 2018-07-05
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================
#pragma once

#include "Helios/System-Libraries.hpp"
#include "Helping-Functions.hpp"
#include "Debugging.hpp"
//########################################################################################

namespace Helios{
//========================================================================================
/*                                                                                      *
 *                                 Forward Declarations                                 *
 *                                                                                      */
//========================================================================================
class Shading_Program;
class Shader;
class Storage_Buffer;
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                   Extra Structures                                   *
 *                                                                                      */
//========================================================================================
/**
 * @brief Enumerator to differentiate a string tag from a long tag
 * 
*/
enum {H_STRING, H_LONG};
/**
 * @brief Union to manage handling both a string path to a source file and an array
 * long offset
 *
*/
union Tag
{
    std::string *path;  //<! Pointer to the path to the texture image
    long id;            //<! Offset in an array
};
/**
 * @brief Wrapping class to handle string and long texture identifiers
 *
*/
struct TextureID
{
    Tag tag;            //<! tag identifier, Can be string pointer or long
    int type=H_STRING;  //<! Store the kind of identifier currently represented
    /**
     * @brief Construct a new TextureID object
     *
    */
    TextureID()
    {
        type = H_STRING;
        tag.path = new std::string("");
    }
    /**
     * @brief Copy constructor
     *
     * @param TID The TextureID object we are copying
    */
    TextureID(const TextureID& TID)
    {
        type = TID.type;
        if(type==H_STRING)
            tag.path = new std::string(*(TID.tag.path));
        else
            tag.id = TID.tag.id;
    }
    /**
     * @brief Destroy the TextureID object
     *
    */
    ~TextureID()
    {
        if(type == H_STRING)
            delete(tag.path);
    }
    /**
     * @brief Overlaoded assignment operator or a TextureID
     *
     * @param TID TextureID we are assigning to this instance
     * @return TextureID& A fully copied instance of a TextureID
    */
    TextureID& operator=(const TextureID& TID)
    {
        type = TID.type;
        if(type==H_STRING)
            tag.path = new std::string((*TID.tag.path));
        else
            tag.id = TID.tag.id;
    }
    /**
     * @brief Overload the = operator to assign string values to TextureIDs
     *
     * @param str String so assign to the TextureID
     * @return TextureID& Return this object with the relevant modifications
    */
    TextureID& operator= (std::string str)
    {delete(tag.path); tag.path = new std::string(str); type=H_STRING; return *this;}
    /**
     * @brief Overload the = operator to assign long values to TextureIDs
     *
     * @param val Long to assign to the TextureID
     * @return TextureID& Return this object with the relevant modifications
    */
    TextureID& operator= (long val)
    { if(type==H_STRING) delete(tag.path); tag.id = val; type=H_LONG; return *this;}
    /**
     * @brief Overloaded string operator to enable string s = TextureID statements
     *
     * @return std::string& The string representing the relevant information
    */
    operator std::string&()
    {
        if(type == H_STRING)
        {
            return *(tag.path);
        }

        std::string *str = new std::string(std::to_string(tag.id));
        return *str;
    }
};
/**
 * @brief Overloaded >> operator for input streams
 *
 * Allows \a inputstream >> TextureID \a statements
 *
 * @param is Input stream
 * @param TID TextureID into which to load the information
 * @return std::istream& The input stream after modification
*/
std::istream inline &operator>> (std::istream& is, TextureID& TID)
{is >> *(TID.tag.path); TID.type = H_STRING; return is;}
/**
 * @brief Overloaded << operator for output streams
 *
 * @param os Output stream
 * @param TID TextureID TextureID from which to load the information
 * @return std::ostream& The output stream afeter modification
*/
std::ostream inline &operator<< (std::ostream& os, TextureID& TID)
{
    if(TID.type==H_STRING)
        return os << *(TID.tag.path);
    else
        return os << TID.tag.id;
}

//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                  Class Declarations                                  *
 *                                                                                      */
//========================================================================================
/**
 * @brief Wrapper class for textures
 *
*/
class Texture
{
//──── Private Members ───────────────────────────────────────────────────────────────────

    protected:
        static GLuint T_VAO;

        GLuint textureID = 0;   //!< OpenGL name of the texture
        GLuint target;          //!< Target of the texture (e.g GL_TEXTURE_2D)

        int color_format;   //!< The color format of the texture (e.g GL_RGBA)
        int width;          //!< width of the texture
        int height;         //!< height of the texture

    public:
//──── Class Functions ───────────────────────────────────────────────────────────────────

        void static assertVAO(){if(T_VAO==0)glCreateVertexArrays(1, &T_VAO);}
//──── Constructors and Destructors ──────────────────────────────────────────────────────

        /**
         * @brief Construct a new Texture object
         * 
        */
        Texture(){}
        /**
         * @brief Construct a new Texture object
         *
         * @param file_path Path to the texture file
         * @param target The OpenGL target texture
        */
        Texture(std::string file_path, GLuint target);
        /**
         * @brief Destroy the Texture object
         *
        */
        ~Texture(){glDeleteTextures(1, &textureID);}

//──── Other Methods ─────────────────────────────────────────────────────────────────────
        /**
         * @brief overlaoded () operator use this instead of the = operator when assigning
         * textures
         *
         * @param o_texture Texture to be copied, will be useless/invalid after statement
         * @return Texture& The new texture
        */
        Texture& operator()(Texture& o_texture)
        {
            target = o_texture.target;

            glDeleteTextures(1, &textureID);
            textureID = o_texture.textureID;
            o_texture.textureID = 0;

            color_format = o_texture.color_format;
            width = o_texture.width;
            height = o_texture.height;

            o_texture.~Texture();
        }
        /**
         * @brief Bind the texture to a texture unit
         * 
         * @param texture_unit 
        */
        void bind_texture(GLuint texture_unit)
        {
            //Bind texture to the texture unit to its appropriate target
            glActiveTexture(GL_TEXTURE0 + texture_unit);
        	glBindTexture(target, textureID);
        }
        /**
         * @brief Draw this image as is to the screen
         *
        */
        void inline draw()
        {
            glBindVertexArray(T_VAO);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }

        GLuint getID(){ return textureID; }
};
/**
 * @brief Class wrapping a texture array to load multiple textures at once to the GPU
 *
 * An array of vec2's will be loaded as an SSBO at binding point 0. The x value is a
 * stretching coefficient in the horizontal direction relative to the maximum stretch of
 * a texture. The y value si the same for the vertical direction.
 *
 * For the x direction, the final value must be computed in the shader as:
 * u = mod(prev_u*stretch.x, stretch.x);
 *
 * This is so that we can load a varying number of textures with different sizes into the
 * array each x and y coefficient represents the relative scaling of that dimension with
 * respect to the maximum dimension across all images.
 *
*/
class Texture2D_Array : public Texture
{
//──── Private members ───────────────────────────────────────────────────────────────────
    private:
        int layer_num;                          //!< Number of layers in the current array
        Storage_Buffer *relative_stretches;     //!< Array of stretching coefficients

    public:

//──── Contructors & Destructors ─────────────────────────────────────────────────────────
        /**
         * @brief Construct a new Texture2D_Array object
         * 
        */
        Texture2D_Array(){}
        /**
         * @brief Construct a new Texture2D_Array object
         *
         * @param texture_files An array of file paths to the texture images
         * @param target the target to which to bind the texture array (should be
         * GL_TEXTURE_2D_ARRAY or GL_TEXTURE_3D)
        */
        Texture2D_Array(std::vector<std::string> texture_files, GLuint target);
        /**
         * @brief Destroy the Texture2D_Array object
         *
        */
        ~Texture2D_Array();

//──── Other Functions ───────────────────────────────────────────────────────────────────
        /**
         * @brief overlaoded () operator use this instead of the = operator when assigning
         * textures
         *
         * @param o_texture Texture to be copied, will be useless/invalid after statement
         * @return Texture& The new texture
        */
        Texture2D_Array& operator()(Texture2D_Array& o_texture)
        {
            layer_num = o_texture.layer_num;
            relative_stretches = o_texture.relative_stretches;
            o_texture.relative_stretches = NULL;
            Texture::operator()(o_texture);
        }
        /**
         * @brief Explicitly binds the texture array to the specified texture unit and
         * implicitly binds the stretch buffer to binding_point 0
         *
         * @param texture_unit Texture unit to bind to
        */
        void bind_texture(GLuint texture_unit);
        /**
         * @brief
         * 
         * @param texture_unit 
         * @param buffer_binding_point 
        */
        void bind_texture(GLuint texture_unit, GLuint buffer_binding_point);
};
/**
 * @brief Wrapper class for a 3D image
 *
*/
class Image3D : public Texture
{

//──── Private Members ───────────────────────────────────────────────────────────────────

    protected:
        int depth; //<! Depth of the 3D image (width and heigh inherited from Texture)

//──── Public Members ────────────────────────────────────────────────────────────────────

    public:
        /**
         * @brief Construct a new 3D Image
         *
         * @param width Width of texture
         * @param height Height of texture
         * @param depth Depth of texture
        */
        Image3D(int width, int height, int depth);
        /**
         * @brief Bind the Image as a texture to the specified texture unit
         *
         * @param texture_unit The texture unit to bind to
        */
        void inline bind_texture(GLuint texture_unit)
        {Texture::bind_texture(texture_unit);}
        /**
         * @brief Bind the image as an image store to the specified image unit
         *
         * @param image_unit The image unit to bind to
        */
        void bind_image(GLuint image_unit)
        {glBindImageTexture(image_unit,textureID,0,GL_TRUE,0,GL_READ_WRITE,GL_RGBA8);}
        /**
         * @brief Bind a single layer of the image as an image to the specified image unit
         * 
         * @param image_unit Image unit to bind to
         * @param layer Layer in the image to bind to the image unit
        */
        void bind_layer_texture(GLuint image_unit, GLuint layer)
        {glBindImageTexture(image_unit,textureID,0,GL_FALSE,layer,GL_READ_WRITE,GL_RGBA8);}
        /**
         * @brief Set the OpenGL Label of the image object
         *
         * @param label The name which will identify this texture
        */
        void inline setLabel(std::string label)
        {
            glBindTexture(target, textureID);
            glObjectLabel(GL_TEXTURE, textureID, -1, ("\""+label+"\"").c_str());
        }
};
/**
 * @brief Struct to represent a material for object rendering
 *
*/
struct Material
{
//──── Fields ────────────────────────────────────────────────────────────────────────────

    std::string name = "No material name";  //<! Name of the material

    glm::vec3 ambient_ref;  //<! ambient reflectivity
    glm::vec3 diffuse_ref;  //<! diffuse_reflectivity (albedo)
    glm::vec3 spec_ref;     //<! specular reflectivity
    glm::vec3 emissiveness; //<! emissiveness of the material

    glm::vec3 transmission_filter;  //<! Transmission filter

    int illum_model = 0;    //<! Illumination model for more information see the
        //<! <a href="linkURL">http://paulbourke.net/dataformats/mtl/ Documentation</a>

    float dissolve = 0;         //<! Dissolve coefficient (merge to background)
    float spec_exponent = 0;    //<! Specular exponent
    float refraction_index = 0; //<! Refraction Index of transparent materials

    TextureID ambient_map;      //<! ambient map texture
    TextureID diffuse_map;      //<! diffuse map texture
    TextureID specular_map;     //<! specular map texture
    TextureID specular_exp_map; //<! specular exponent map texture
    TextureID dissolve_map;     //<! dissolve map texture
    TextureID bump_map;         //<! bump map texture

//──── Constructors & Destructors ────────────────────────────────────────────────────────
    /**
     * @brief Construct a new Material object
     *
    */
    Material();
    /**
     * @brief Construct a new Material object from a string
     *
     * The string has to follow a specific format, basically the one found in an mtl file
     * for a single material.
     *
     * @param material_info String containing the material's info
    */
    Material(std::string material_info);
};
/**
 * @brief Class to wrap a generic 3D mesh
 *
*/
class Mesh
{

//──── Private Members ───────────────────────────────────────────────────────────────────

    private:

        GLuint VAO;         //!< Vertex array object
        GLuint buffers[4];  //!< VBO identifiers

        std::vector<glm::vec3> vertices;    //!< Array of vertices of the mesh
        std::vector<glm::vec3> normals;     //!< Array of normals of the mesh
        std::vector<glm::vec3> uvs;         //!< Array of texture coordinates of the mesh
        std::vector<uint> indices;          //!< Array of indices for per element indexing

    public:

//──── Constructors and Destructors ──────────────────────────────────────────────────────

        /**
         * @brief Construct a new Mesh object
         *
        */
        Mesh();
        /**
         * @brief Construct a new Mesh object
         *
         * @param string path to a wavefront (.obj) file
        */
        Mesh(std:: string file_path);
        /**
         * @brief Construct a new Mesh object from an obj file and a material list
         *
         * @param file_path Files specifying the geometry
         * @param material_list List of amterials to apply to this texture
        */
        Mesh(std::string file_path, std::vector<Helios::Material> material_list);
        /**
         * @brief Destroy the Mesh object
         *
        */
        ~Mesh();

//──── Fundamental Methods ───────────────────────────────────────────────────────────────
        /**
         * @brief Overloaded assignment operator
         *
         * @param o_mesh Mesh to copy from
         * @return Mesh&  Copied mesh
        */
        Mesh& operator=(const Mesh& o_mesh);

//──── GPU related methods ───────────────────────────────────────────────────────────────

        /**
         * @brief Draw the mesh
         *
        */
        void draw();
        /**
         * @brief Load mesh information from a wavefront file
         *
         * @param file_path Path to the .obj file
        */
        void load_from_obj(std::string file_path);

        void load_from_obj(std::string file_path, std::vector<Material> mtl_list);
};
class Transform
{

};
//TODO: Document this class
class Object_Template
{
    private:
        std::vector<Material> materials;
        Mesh mesh;
        Texture2D_Array diffuse_maps;

    public:
        Object_Template(std::string wavefront_file);

        void bind_default();
        void bind_texture_default(GLuint bbpoint);
        void bind_texture_default(GLuint texture_unit, GLuint bbpoint);
        void draw();

};
//########################################################################################
}//Close helios namespace