//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @brief Implementation of OpenGL debug functions
 * 
 * @file Debug.cpp
 * @author Camilo Talero
 * @date 2018-04-19
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//========================================================================================
/*                                                                                      *
 *                                     Include Files                                    *
 *                                                                                      */
//========================================================================================
#include "Debugging.hpp"

namespace Helios{
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                 Function Definitions                                 *
 *                                                                                      */
//========================================================================================

std::string to_hex_string(int val)
{
    std::stringstream stream;
    stream << "0x" << std::hex << val;
    return std::string(stream.str());
}

//Texture enumerator
std::string textureTargetEnumToString(GLenum text)
{
    switch(text)
    {
        case GL_TEXTURE_1D:                     return "GL_TEXTURE_1D";
        case GL_TEXTURE_2D:                     return "GL_TEXTURE_2D";
        case GL_TEXTURE_3D:                     return "GL_TEXTURE_3D";

        case GL_TEXTURE_1D_ARRAY:               return "GL_TEXTURE_1D_ARRAY";
        case GL_TEXTURE_2D_ARRAY:               return "GL_TEXTURE_2D_ARRAY";

        case GL_TEXTURE_RECTANGLE:              return "GL_TEXTURE_RECTANGLE";
        case GL_TEXTURE_CUBE_MAP:               return "GL_TEXTURE_CUBE_MAP";
        case GL_TEXTURE_CUBE_MAP_ARRAY:         return "GL_TEXTURE_CUBE_MAP_ARRAY";
        case GL_TEXTURE_BUFFER:                 return "GL_TEXTURE_BUFFER";
        case GL_TEXTURE_2D_MULTISAMPLE:         return "GL_TEXTURE_2D_MULTISAMPLE";
        case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:   return "GL_TEXTURE_2D_MULTISAMPLE_ARRAY";
        default:
            return "Not a texture enumerator: " + to_hex_string(text);
    }
}

//Error enumerator
std::string errEnumToString(GLenum err)
{
    switch (err)
    {
        case GL_NO_ERROR:                      return "GL_NO_ERROR";
        case GL_INVALID_ENUM:                  return "GL_INVALID_ENUM";
        case GL_INVALID_VALUE:                 return "GL_INVALID_VALUE";
        case GL_INVALID_OPERATION:             return "GL_INVALID_OPERATION";
        case GL_STACK_OVERFLOW:                return "GL_STACK_OVERFLOW";
        case GL_STACK_UNDERFLOW:               return "GL_STACK_UNDERFLOW";
        case GL_OUT_OF_MEMORY:                 return "GL_OUT_OF_MEMORY";
        case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
        case 0x8031: /* not core */            return "GL_TABLE_TOO_LARGE_EXT";
        case 0x8065: /* not core */            return "GL_TEXTURE_TOO_LARGE_EXT";
        //Warning: The codes below have no documentation I invented the enumerator names
        case 0x20071: /* Nvidia driver code */ return "GL_NV_BIND_BUFFER_NOTIFICATION(*)";
        case 0x20072: /* Nvidia driver code */ return "GL_NV_COPY_VRAM_TO_RAM_WARNING(*)"; 
        default:
            return "Unkown error code: " + to_hex_string(err);
    }
}

//Error source
std::string errSourceToString(GLenum errorS)
{
    switch(errorS)
    {
        case GL_DEBUG_SOURCE_API:               return "GL_DEBUG_SOURCE_API";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:     return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
        case GL_DEBUG_SOURCE_SHADER_COMPILER:   return "GL_DEBUG_SOURCE_SHADER_COMPILER";
        case GL_DEBUG_SOURCE_THIRD_PARTY:       return "GL_DEBUG_SOURCE_THIRD_PARTY";
        case GL_DEBUG_SOURCE_APPLICATION:       return "GL_DEBUG_SOURCE_APPLICATION";
        case GL_DEBUG_SOURCE_OTHER:             return "GL_DEBUG_SOURCE_OTHER";
        default:
            return "Unkown error source enumerator: " + to_hex_string(errorS);
    }
}

//Error type
std::string errTypeToString(GLenum errorT)
{
    switch(errorT)
    {
        case GL_DEBUG_TYPE_ERROR:               return "GL_DEBUG_TYPE_ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PORTABILITY:         return "GL_DEBUG_TYPE_PORTABILITY";
        case GL_DEBUG_TYPE_PERFORMANCE:         return "GL_DEBUG_TYPE_PERFORMANCE";
        case GL_DEBUG_TYPE_MARKER:              return "GL_DEBUG_TYPE_MARKER";
        case GL_DEBUG_TYPE_PUSH_GROUP:          return "GL_DEBUG_TYPE_PUSH_GROUP";
        case GL_DEBUG_TYPE_POP_GROUP:           return "GL_DEBUG_TYPE_POP_GROUP";
        case GL_DEBUG_TYPE_OTHER:               return "GL_DEBUG_TYPE_OTHER";
        default:
            return "Unkown error type enumerator: " + to_hex_string(errorT);
    }
}

//Error severity
std::string errSeverityToString(GLenum errorS)
{
    switch(errorS)
    {
        case GL_DEBUG_SEVERITY_HIGH:            return "GL_DEBUG_SEVERITY_HIGH";
        case GL_DEBUG_SEVERITY_MEDIUM:          return "GL_DEBUG_SEVERITY_MEDIUM";
        case GL_DEBUG_SEVERITY_LOW:             return "GL_DEBUG_SEVERITY_LOW";
        case GL_DEBUG_SEVERITY_NOTIFICATION:    return "GL_DEBUG_SEVERITY_NOTIFICATION";
        default:
            return "Unkown severity enumerator: " + to_hex_string(errorS);
    }
}

//Shader type
std::string shaderEnumToString(GLenum shader_type)
{
    switch(shader_type)
    {
        case GL_VERTEX_SHADER:          return "GL_VERTEX_SHADER";
        case GL_TESS_CONTROL_SHADER:    return "GL_TESS_CONTROL_SHADER";
        case GL_TESS_EVALUATION_SHADER: return "GL_TESS_EVALUATION_SHADER";
        case GL_GEOMETRY_SHADER:        return "GL_GEOMETRY_SHADER";
        case GL_FRAGMENT_SHADER:        return "GL_FRAGMENT_SHADER";
        case GL_COMPUTE_SHADER:         return "GL_COMPUTE_SHADER";
        default:
            return "Not a shader enumerator value: " + to_hex_string(shader_type);
    }
}

//Error message generation
void errorCallback(   GLenum source, GLenum type, GLuint id, GLenum severity,
                        GLsizei length, const GLchar* message, const void* userParam )
{
   //if(id == 131186 || id == 131185) return;
    std::string wMessage = Log::wrap_text(std::string(message), 80);
    std::cerr << "OpenGL Event Notification:\n" + wMessage << std::endl;

    char frame = '!';
    if(severity == GL_DEBUG_SEVERITY_NOTIFICATION)
        frame = '-';

    std::string error_message = std::string(80, frame) + "\n";
    error_message += "OpenGL Event Notification:\n";
    error_message += "Source: " + errSourceToString(source) + "\n";
    error_message += "Type: " + errTypeToString(type) + "\n";
    error_message += "ID: " + errEnumToString(id) + "\n";
    error_message += "Severity: " + errSeverityToString(severity) + "\n";
    error_message += "Message:\n" + wMessage + "\n";
    error_message += Log::wrap_text(
                    "\n\nNote: If (*) is in the enumerator name. "
                    "These are not the official names for these enumerators."
                    " No documentation on the matter was found so they were given " 
                    "descriptive names but these names do not exist outside of Helios", 80);
    error_message += std::string(80, frame);

    Log::record_log(error_message);
}
}//Helios namespace closing bracket
//########################################################################################