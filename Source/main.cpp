//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * @brief Main file to start execution of the program
 *
 * @file main.cpp
 * @author Camilo Talero
 * @date 2018-04-14
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//========================================================================================
/*                                                                                      *
 *                                  Includes and Macros                                 *
 *                                                                                      */
//========================================================================================
#include "Nyx.hpp"
#include "Helios.hpp"
#include <unistd.h>

#define RESOLUTION 128
using namespace std;
using namespace glm;
//########################################################################################

//========================================================================================
/*                                                                                      *
 *                                         Main                                         *
 *                                                                                      */
//========================================================================================

enum {BASIC=0, VOXELIZER, RAY_TRACER, IMAGE, PROJECTION};
vector<Helios::Shading_Program*> programs(5);

Helios::Object_Template *object;
Helios::Camera c;
Helios::Image3D *diffuse_map;
Helios::Image3D *normal_map;
Helios::Storage_Buffer *voxel_buffer;
Helios::Storage_Buffer *tree_buffer;
Nyx::Nyx_Keyboard* kbd;
Nyx::Nyx_Mouse* mouse;
Nyx::Nyx_Window* w;

Helios::Texture *t;

GLuint fbuffer;
GLuint text;

int shading_model = 0;

GLuint TID, ssbo;
float val = 0;
float val2 = 0;
float val3 =0;
int num = 0;
int num2 = 0;
int num3 = 0;

void voxels()
{
    int width, height;
    glfwGetWindowSize(w->getWindowPtr(), &width, &height);
    glViewport(0,0,width,height);
    programs[RAY_TRACER]->use();
    glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    glFinish();
    //glGenerateTextureMipmap(diffuse_map->getID());
    //glGenerateTextureMipmap(normal_map->getID());
    tree_buffer->set_data(0);
    voxel_buffer->set_data(1);
}

void normal()
{
    int width, height;
    glfwGetWindowSize(w->getWindowPtr(), &width, &height);
    glViewport(0,0,width,height);
    programs[PROJECTION]->use();
    glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
    object->draw();
}

void both()
{
    int width, height;
    glfwGetWindowSize(w->getWindowPtr(), &width, &height);
    glViewport(0,0,width/2,height);
    programs[RAY_TRACER]->use();
    glDrawArrays(GL_TRIANGLE_STRIP,0,4);

    width, height;
    glfwGetWindowSize(w->getWindowPtr(), &width, &height);
    glViewport(width/2,0,width/2,height);
    programs[PROJECTION]->use();
    glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
    object->draw();
}

void render()
{
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    kbd->updateAllKeys();
    c.load_to_program(programs[BASIC]);
    c.load_to_program(programs[PROJECTION]);

    programs[RAY_TRACER]->load_uniform(c.getPosition(), "camera_pos");

    programs[VOXELIZER]->use();
    glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);
    glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
    glViewport(0, 0, RESOLUTION, RESOLUTION);
    object->draw();
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    if(shading_model==0)
        voxels();
    if(shading_model==1)
        normal();
    if(shading_model==2)
        both();

    //vec3 pos = c.getPosition();
    //cout << pos << endl;
    //usleep( 10'000);
}
#define CAM_SPEED 10.0f
#include <glm/gtx/transform.hpp>
#include "Geometry-Wrappers.hpp"
mat4 rot = mat4(1);
void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    glfwSetCursorPos(window, width/2, height/2);

    c.rotateH(0.0001*(width/2 - xpos));
    c.rotateV(0.0001*(height/2 - ypos));
        vec3 side = c.getSide();

    rot = glm::rotate(rot, (float)(0.0001f*(width/2.f - xpos)), vec3(0,1,0));

    programs[RAY_TRACER]->load_uniform(rot, "view_m");
}

void set_mouse(Nyx::Nyx_Window *w)
{
    mouse = new Nyx::Nyx_Mouse(w);
    mouse->set_pos_func(cursor_position_callback);
}
void set_keyboard_keys(Nyx::Nyx_Window *w)
{
    kbd = new Nyx::Nyx_Keyboard(w);
    kbd->set_1_func([]()->void{shading_model=0;});
    kbd->set_2_func([]()->void{shading_model=1;});
    kbd->set_3_func([]()->void{shading_model=2;});

    kbd->set_w_func([]()->void{c.translateForward(CAM_SPEED);});
    kbd->set_s_func([]()->void{c.translateForward(-CAM_SPEED);});
    kbd->set_d_func([]()->void{c.translateSideways(-CAM_SPEED);});
    kbd->set_a_func([]()->void{c.translateSideways(CAM_SPEED);});
    kbd->set_shift_func([]()->void{c.translate(vec3(0,-1,0)*CAM_SPEED);});
    kbd->set_space_func([]()->void{c.translate(vec3(0,1,0)*CAM_SPEED);});
}

void set_shaders()
{
    programs[BASIC] = new Helios::Shading_Program(
        3,
        "VGI-Experimental/Basic-Vertex.glsl",
        "VGI-Experimental/Basic-Geometry.glsl",
        "VGI-Experimental/Basic-Fragment.glsl");
    programs[BASIC]->set_program_name("Basic");

    programs[VOXELIZER] = new Helios::Shading_Program(
        3,
        "VGI-Shaders/Voxelization-vertex.glsl",
        "VGI-Shaders/Voxelization-geometry.glsl",
        "VGI-Shaders/Voxelization-fragmentp2.glsl");

    programs[RAY_TRACER] = new Helios::Shading_Program(
        2,
        "VGI-Shaders/VoxelRayTracing-vertex.glsl",
        "VGI-Shaders/VoxelRayTracing-fragmentp2.glsl");

    programs[IMAGE] = new Helios::Shading_Program(
        2,
        "VGI-Experimental/Layer-Vertex.glsl",
        "VGI-Experimental/Layer-Fragment.glsl");

    programs[PROJECTION] = new Helios::Shading_Program(
        2,
        "VGI-Shaders/Projection-Vertex.glsl",
        "VGI-Shaders/Projection-Fragmentp2.glsl");

    float cube_dim = 1500.f;
    programs[VOXELIZER]->load_uniform(RESOLUTION, "voxel_resolution");
    programs[VOXELIZER]->load_uniform(cube_dim, "cube_dim");
    int x,y;
    w->getDimensions(&x,&y);
    programs[RAY_TRACER]->load_uniform((float)x/(float)y, "aspect_ratio");
    programs[RAY_TRACER]->load_uniform(RESOLUTION, "voxel_resolution");
    programs[RAY_TRACER]->load_uniform(cube_dim, "cube_dim");

    programs[PROJECTION]->load_uniform(RESOLUTION, "voxel_resolution");
    programs[PROJECTION]->load_uniform(cube_dim, "cube_dim");
}

void inline load_screen(Nyx::Nyx_Window *w)
{
    t = new Helios::Texture("Assets/galaxy.jpg", GL_TEXTURE_2D);
    t->bind_texture(2);
    programs[IMAGE]->use();
    t->draw();
    glfwSwapBuffers(w->getWindowPtr());
}

extern vector<string> parse_materials(string file_path, vector<Helios::Material> &material_list);
int main()
{
    Nyx::NyxInit(NYX_TOLERANCE_HIGH);
    w = new Nyx::Nyx_Window("Example", render, NULL, true);

    c.setPosition(vec3(0,0,-10));

    Helios::HeliosInit();

    set_shaders();
    set_keyboard_keys(w);
    set_mouse(w);

    load_screen(w);

    vector<Helios::Material> material_list;

    //vector<string> files = parse_materials("Assets/sponza.mtl", material_list);
    ////make textures
    //diffuse_map = new Helios::Image3D(RESOLUTION, RESOLUTION, RESOLUTION);
    //diffuse_map->setLabel("diffuse_map");
    //normal_map = new Helios::Image3D(RESOLUTION, RESOLUTION, RESOLUTION);
    //normal_map->setLabel("normal_map");

    ////bind textures
    //diffuse_map->bind_image(0);
    //diffuse_map->bind_texture(0);
    //normal_map->bind_image(1);
    //normal_map->bind_texture(1);

    glGenTextures(1, &text);
    glGenFramebuffers(1, &fbuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, fbuffer);
    glBindTexture(GL_TEXTURE_2D, text);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, RESOLUTION, RESOLUTION, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, text, 0);

    object = new Helios::Object_Template("Assets/sponza.obj");
    object->bind_texture_default(2,2);

    //glGenerateTextureMipmap(diffuse_map->getID());
    //glGenerateTextureMipmap(normal_map->getID());

    voxel_buffer = new Helios::Storage_Buffer(2'000'000'000);
    voxel_buffer->bind_buffer(0);
    tree_buffer = new Helios::Storage_Buffer(2'000'000'000);
    tree_buffer->bind_buffer(1);
    tree_buffer->set_data(1);

    w->start_loop();

    for(Helios::Shading_Program* p: programs)
        delete(p);
}
//########################################################################################
